#add_subdirectory("rndfield-mimcmc")
add_subdirectory("muq2-rndfield-mlmcmc")
add_subdirectory("modelling")


#find_package(MUQ_PRIVATE REQUIRED)
#include_directories(${MUQ_INCLUDE_DIRS})

#message("Using ${MUQ_LIBRARIES}")


#add_executable("dune-muq" dune-muq.cc)
#target_link_dune_default_libraries("dune-muq")
#target_link_libraries("dune-muq" ${MUQ_LIBRARIES} ${MUQ_LINK_LIBRARIES})

#add_executable("conductivity-mcmc" conductivity-mcmc.cc)
#target_link_dune_default_libraries("conductivity-mcmc")
#target_link_libraries("conductivity-mcmc" ${MUQ_LIBRARIES} ${MUQ_LINK_LIBRARIES})

#add_executable(example02 example02.cc)
#target_link_dune_default_libraries(example02)
#target_link_libraries(example02 ${MUQ_LIBRARIES} ${MUQ_LINK_LIBRARIES})

#add_executable(dune-modelling dune-modelling.cc)
#target_link_dune_default_libraries(dune-modelling)
#target_link_libraries(dune-modelling ${MUQ_LIBRARIES} ${MUQ_LINK_LIBRARIES})
#dune_symlink_to_source_files(FILES modelling.ini boundary.groundwater boundary.transport1
#  boundary.transport2 conductivity.field storativity.field porosity.field)


# Link to config
dune_symlink_to_source_files(FILES config.ini)

dune_symlink_to_source_files(FILES plot.plt)
