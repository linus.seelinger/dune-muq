#include "MUQ/SamplingAlgorithms/ParallelizableMIComponentFactory.h"


const int GRID_SIZE[2] = {32, 64};
//const int GRID_SIZE[3] = {16, 32, 64};
const int QOI_GRID_SIZE = 32;
const int LEVELS1 = 2;
//const int LEVELS1 = 3;

#include "MUQ/SamplingAlgorithms/ConcatenatingInterpolation.h"


int paramSize (std::shared_ptr<MultiIndex> index) {
  //const int PARAM_SIZE[2] = {4, 7}; // 5, 7
  //std::cout << "size: " << 4*std::pow(2*PARAM_SIZE[index->GetValue(0)]+1,2) << std::endl;
  //return configuration.get<int>("param_size" + std::to_string(index->GetValue(0)), 0);//2*std::pow(2*PARAM_SIZE[index->GetValue(0)]+1,2);

  int layers = configuration.get<int>("param_size" + std::to_string(index->GetValue(0)), 0);
  //return 2 * std::pow(layers, 2) - 1;
  //return 2 * (1 + 2 * layers) * (1 + layers) - 1;
  //return 2 * std::pow(1 + 2 * layers, 2) - 1;
  return 2 * layers * (layers - 1) + 1;
}




class MySamplingProblem : public AbstractSamplingProblem {
public:

  //Eigen::VectorXi QOI_SIZE;

  MySamplingProblem(std::shared_ptr<MultiIndex> index, std::shared_ptr<parcer::Communicator> comm, std::shared_ptr<Eigen::VectorXd> measurements_external_in)
   : cells(GRID_SIZE[index->GetValue(0)]),

     // FIXME: Aus irgendeinem #~';"%_ Grund kann cells nicht als Argument für QOI-Größe genutzt werden??
     //AbstractSamplingProblem(Eigen::VectorXi::Constant(1,paramSize(index)), Eigen::VectorXi::Constant(1,GRID_SIZE*GRID_SIZE)),
     AbstractSamplingProblem(Eigen::VectorXi::Constant(1,paramSize(index)), Eigen::VectorXi::Constant(1,QOI_GRID_SIZE*QOI_GRID_SIZE)),
     index(index),
     comm(comm),
     problem(index, comm),
     measurements_external(measurements_external_in)
  {

    int overlap = 1;//configuration.get<int>("Grid.overlap", 1);

    qoi = Eigen::VectorXd::Zero(QOI_GRID_SIZE*QOI_GRID_SIZE);//(GRID_SIZE+2)*(GRID_SIZE+2));


    Dune::FieldVector<double,dim> L(1.0);
    std::array<int,dim> N;
    std::fill(N.begin(), N.end(), cells);
    typedef Dune::YaspFixedSizePartitioner<dim> YP;
    std::array<int,dim> yasppartitions = {comm->GetSize(), 1};
    //auto yp = new YP(yasppartitions);

    std::bitset<dim> B(false);
    grid = std::make_shared<GM>(L,N,B,overlap,comm->GetMPICommunicator());//, yp);
    grid->loadBalance();

    gv = std::make_shared<GV>(grid->levelGridView(grid->maxLevel()));

    fem = std::make_shared<FEM>(Dune::GeometryType(Dune::GeometryType::cube, dim));

    gfs = std::make_shared<GFS>(*gv,*fem);
    gfs->update();


    //for (int x : {4, 12, 20, 28}) {
    //  for (int y : {4, 12, 20, 28}) {
    //for (int x : {5, 11, 17, 23, 29}) {
    //  for (int y : {5, 11, 17, 23, 29}) {
    //for (int x : {1, 9, 17, 24, 31}) {
      //for (int y : {1, 9, 17, 24, 31}) {
    for (int x : {2, 7, 13, 19, 25, 30}) {
      for (int y : {2, 7, 13, 19, 25, 30}) {

    //for (int x : {2, 8, 12, 16, 20, 24, 30}) {
    //  for (int y : {2, 8, 12, 16, 20, 24, 30}) {

//    for (int x : {2, 4, 6, 8, 10, 12}) {
  //    for (int y : {2, 4, 6, 8, 10, 12}) {
    //for (int x : {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30}) {
    //  for (int y : {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30}) {
    //for (int x : {2, 6, 10, 14, 18, 22, 26, 30}) {
        //  for (int y : {2, 6, 10, 14, 18, 22}) {
        measurements.push_back({(double)x/32.0,(double)y/32.0,0,0});
      }
    }

    if (measurements_external == nullptr) {
      std::cout << "Computing measurements for " << *index << std::endl;
      computeMeasurements();
      measurements_external = std::make_shared<Eigen::VectorXd>(measurements.size() * 2);

      int c = 0;
      for (std::vector<double>& measurement : measurements) {
        (*measurements_external)(c) = measurement[2];
        (*measurements_external)(c+1) = measurement[3];
        c += 2;
      }

      //std::cout << "done" << *index << std::endl;
    } else {
      //std::cout << "Unpacking measurements for " << *index << std::endl;

      int c = 0;
      for (std::vector<double>& measurement : measurements) {
        measurement[2] = (*measurements_external)(c);
        measurement[3] = (*measurements_external)(c+1);
        c += 2;
      }
      //std::cout << "done" << *index << std::endl;
    }
  }

  virtual ~MySamplingProblem() = default;


  virtual std::shared_ptr<SamplingState> QOI() override {

    return std::make_shared<SamplingState>(qoi, 1.0);
  }

  std::shared_ptr<Eigen::VectorXd> GetMeasurements() {
    assert(measurements_external != nullptr);
    return measurements_external;
  }



private:
  // define parameters
  static const unsigned int dim = 2;
  static const unsigned int degree = 1;
  const std::size_t nonzeros = std::pow(2*degree+1,dim);
  static const Dune::GeometryType::BasicType elemtype = Dune::GeometryType::cube;
  static const Dune::PDELab::MeshType meshtype = Dune::PDELab::MeshType::conforming;
  //static const Dune::SolverCategory::Category solvertype = Dune::SolverCategory::overlapping;//Dune::SolverCategory::overlapping;
  static const Dune::SolverCategory::Category solvertype = Dune::SolverCategory::sequential;//Dune::SolverCategory::overlapping;
  typedef double NumberType;
  typedef Dune::YaspGrid<dim> GM;
  typedef typename GM::LevelGridView GV;
  typedef GenericEllipticProblem<GM::LevelGridView,NumberType> Problem;
  typedef Problem::RndFieldTraits::RangeType RangeType;
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<Problem> BCType;
  typedef Dune::PDELab::CGSpace<GM,NumberType,degree,BCType,elemtype,meshtype,solvertype> FS;

  using GridTraits = typename Problem::MyGridTraits;
  using DomainField = typename GridTraits::DomainField;
  using RangeField  = typename GridTraits::RangeField;
  using FEM = Dune::PDELab::P0LocalFiniteElementMap<DomainField,RangeField,dim>;

  // make grid function space
  typedef Dune::PDELab::NoConstraints CON;
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
public:
  typedef FS::DOF V;

  void test(std::shared_ptr<SamplingState> state, std::string filename, double& density) {
    /*if (comm->GetRank() == 0) {
      for (int dest = 1; dest < comm->GetSize(); dest++) {
        comm->Send(ControlFlag::TEST, dest, WorkgroupTag);
        comm->Send(id, dest, WorkgroupTag);
        comm->Send(state->state[0], dest, WorkgroupTag);
      }
    }*/
    bool converged;
    density = solvePDE(state->state, converged, false, filename, "", true);
    //LogDensityImpl(state, density, filename);
  }


  void writeVTKfromQOI(Eigen::VectorXd data, std::string filename) {
    if (comm->GetRank() == 0) {

      Dune::FieldVector<double,dim> L(1.0);
      std::array<int,dim> N;
      std::fill(N.begin(), N.end(), QOI_GRID_SIZE);
      const int overlap = 0;
      std::bitset<dim> B(false);

      auto qoi_grid = std::make_shared<GM>(L,N,B,overlap,MPI_COMM_SELF);
      auto qoi_gv = std::make_shared<GV>(qoi_grid->levelGridView(qoi_grid->maxLevel()));

      auto qoi_gfs = std::make_shared<GFS>(*qoi_gv,*fem);
      qoi_gfs->update();


      typedef double Real;

      // make a degree of freedom vector
      using U = Dune::PDELab::Backend::Vector<GFS,Real>;
      U u(*qoi_gfs,0.0);

      assert(u.N() == data.rows());

      int c = 0;
      for (Real& val : u) {
        val = data(c);
        c++;
      }

      Dune::VTKWriter<GV> vtkwriter(qoi_gfs->gridView());
      typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
      DGF xdgf(*qoi_gfs,u);
      typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> ADAPT;
      auto adapt = std::make_shared<ADAPT>(xdgf,"perm_field");
      vtkwriter.addVertexData(adapt);
      vtkwriter.write(filename);

    }
  }

  int localDOFs() {
    return qoi_local.rows();
  }


//private:

  std::vector<std::vector<double>> measurements;


  virtual double LogDensity(std::shared_ptr<SamplingState> const& state) override {

    //std::cout << "Sequential LogDensity eval" << std::endl;

    std::stringstream ss;
    ss << std::setw(6) << std::setfill('0') << cnt;

    double density;
    LogDensityImpl (state, density, "", "_lvl" + std::to_string(index->GetValue(0)) + "_s" + ss.str());

    cnt++;

    return density;
  }

  void LogDensityImpl(std::shared_ptr<SamplingState> state, double& density, std::string prefix = "", std::string suffix = "") {

    bool converged;
    density = solvePDE(state->state, converged, false, prefix, suffix);

    if (!converged) {
      density = -1e15;
      std::cout << "NOT CONVERGED!" << std::endl;
    }
  }

  void computeMeasurements() {

    std::cout << "set reference true" << std::endl;
    //std::uniform_real_distribution<double> unif(lower_bound,upper_bound);
    std::normal_distribution<double> distribution(0.0,1.0);
    std::default_random_engine re;

    Eigen::VectorXd ref_param = Eigen::VectorXd::Zero(paramSize(index));
    for (int i = 4; i < paramSize(index); i++) {//ref_param.size(); i++) {
      //ref_param[i] += 1.0;
      ref_param[i] = 50.0 * distribution(re) / ((double)i*configuration.get<double>("ref_mode_decay", 0.1)+1.0);
      //if (i >= std::pow(PARAM_SIZE[0],2))
      //  ref_param[i] *= 0.05;
      //ref_param[i] *= 3.0;// / (i/5.0+1);
    }
    //ref_param.setRandom();
    std::vector<Eigen::VectorXd> ref_paramv;
    ref_paramv.push_back(ref_param);
    bool converged;
    solvePDE(ref_paramv, converged, true, "reference");

    //problem.field->writeToVTK("reference field", "perm_field", grid->levelGridView(grid->maxLevel()));

    std::cout << "reference done" << std::endl;

  }

  double solvePDE(std::vector<Eigen::VectorXd> const& input, bool& converged, bool reference_run, std::string prefix = "", std::string suffix = "", bool vtkoutput = false) {


    problem.setParameters (input);
    problem.setReference (reference_run);

    // make a finite element space
    int J = grid->maxLevel();
    BCType bctype(grid->levelGridView(J),problem);
    FS fs(*grid,bctype);
    using Dune::PDELab::Backend::native;

    // Extract field to QOI
    {
      typedef double Real;

      auto field_function = Dune::Functions::makeAnalyticGridViewFunction([&](auto x)
      { RangeType output;
        problem.perm(x,output);

        return output;},*gv);

      // make a degree of freedom vector
      using U = Dune::PDELab::Backend::Vector<GFS,Real>;
      U u(*gfs,0.0);

      // interpolate from a given function
      Dune::PDELab::interpolate(field_function,*gfs,u);

      typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
      DGF xdgf(*gfs,u);

      int c = 0;
      //for (double y = 1E-10; y <= 1.0; y += 1.0 / (QOI_GRID_SIZE-1) - 1E-8) {
      //  for (double x = 1E-10; x <= 1.0; x += 1.0 / (QOI_GRID_SIZE-1) - 1E-8) {
      for (double y = 0.0; y < 1.0-1E-7; y += 1.0 / QOI_GRID_SIZE) {
        for (double x = 0.0; x < 1.0-1E-7; x += 1.0 / QOI_GRID_SIZE) {
          DGF::Traits::DomainType location;
          location[0] = x;//std::min(1.0-1E-8, x + 1E-8);
          location[1] = y;//std::min(1.0-1E-8, y + 1E-8);
          Dune::PDELab::GridFunctionProbe<DGF> probe(xdgf, location);

          DGF::Traits::RangeType val;
          probe.eval_all(val);
          qoi(c) = val[0];
          c++;
        }
      }
      assert(c == qoi.rows());


      if (reference_run || vtkoutput) {
        //problem.field->writeToVTK("perm_field", grid->levelGridView(J));
        std::string s = "perm_field" + prefix + suffix;
        //problem.field->writeToVTK(s, "perm_field", grid->levelGridView(J));

        Dune::VTKWriter<GV> vtkwriter(gfs->gridView());
        typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
        DGF xdgf(*gfs,u);
        typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> ADAPT;
        auto adapt = std::make_shared<ADAPT>(xdgf,"perm_field");
        vtkwriter.addVertexData(adapt);
        vtkwriter.write(s);

      }

    }

    // make a degree of freedom vector and initialize it with a function
    V x(fs.getGFS(),0.0);
    typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<Problem> G;
    G g(grid->levelGridView(J),problem);
    Dune::PDELab::interpolate(g,fs.getGFS(),x);

    // assemble constraints
    fs.assembleConstraints(bctype);
    fs.setNonConstrainedDOFS(x,0.0);

    // assembler for finite elemenent problem
    typedef Dune::PDELab::ConvectionDiffusionFEM<Problem,FS::FEM> LOP;
    LOP lop(problem);
    typedef Dune::PDELab::GalerkinGlobalAssembler<FS,LOP,solvertype> ASSEMBLER;
    ASSEMBLER assembler(fs,lop, nonzeros);

    // make linear solver and solve problem
    //typedef Dune::PDELab::ISTLSolverBackend_IterativeDefault<FS,ASSEMBLER,solvertype> SBE;
    typedef Dune::PDELab::ISTLSolverBackend_CG_SSOR<FS,ASSEMBLER,solvertype> SBE;
    //typedef Dune::PDELab::ISTLSolverBackend_CG_AMG_SSOR<FS,ASSEMBLER,solvertype> SBE;
    //typedef Dune::PDELab::ISTLBackend_SEQ_UMFPack<FS,ASSEMBLER,solvertype> SBE;
    //typedef Dune::PDELab::ISTLSolverBackend_ExplicitDiagonal<FS,ASSEMBLER,solvertype> SBE;
    SBE sbe(fs,assembler,500,0);
    typedef Dune::PDELab::StationaryLinearProblemSolver<ASSEMBLER::GO,SBE::LS,V> SLP;
    SLP slp(*assembler,*sbe,x,1e-6,1e-99,0);
    slp.apply();

    converged = slp.ls_result().converged;


    {


      if (reference_run || vtkoutput) {

        Dune::SubsamplingVTKWriter<GM::LeafGridView> vtkwriter(grid->leafGridView(),Dune::refinementLevels(degree-1));
        //FS::DGF xdgf_m(fs.getGFS(),meas);
        //vtkwriter.addVertexData(std::make_shared<FS::VTKF>(xdgf_m,"measurements"));

        FS::DGF xdgf(fs.getGFS(),x);
        vtkwriter.addVertexData(std::make_shared<FS::VTKF>(xdgf,"solution"));

        std::string s = "solution" + prefix + suffix;
        vtkwriter.write(s,Dune::VTK::appendedraw);
      }

    }

    if (reference_run) {
      for (std::vector<double>& measurement : measurements) {

        typedef Dune::PDELab::DiscreteGridFunction<V::GridFunctionSpace,V> DGF;
        DGF xdgf(x.gridFunctionSpace(),x);

        DGF::Traits::DomainType location;
        location[0] = double(measurement[0]);// / (double)GRID_SIZE;
        location[1] = double(measurement[1]);// / (double)GRID_SIZE;
        Dune::PDELab::GridFunctionProbe<DGF> probe(xdgf, location);

        DGF::Traits::RangeType val;
        probe.eval_all(val);
        measurement[2] = val[0];

        double perm_val;
        if (probe.getEvalRank() == comm->GetRank()) {
          RangeType output;
          problem.perm(location,output);
          perm_val = output;
        }
        comm->Bcast(perm_val, probe.getEvalRank());

        measurement[3] = perm_val;

      }
      return -1.0;
    } else {
      double normdiff = 1e-10;

      for (std::vector<double>& measurement : measurements) {
        typedef Dune::PDELab::DiscreteGridFunction<V::GridFunctionSpace,V> DGF;
        DGF xdgf(x.gridFunctionSpace(),x);

        DGF::Traits::DomainType location;
        location[0] = double(measurement[0]);
        location[1] = double(measurement[1]);
        Dune::PDELab::GridFunctionProbe<DGF> probe(xdgf, location);

        DGF::Traits::RangeType val;
        probe.eval_all(val);

        normdiff += std::pow(val[0] - measurement[2],2);

        double perm_val;
        if (probe.getEvalRank() == comm->GetRank()) {
          RangeType output;
          problem.perm(location,output);
          perm_val = output;
        }
        comm->Bcast(perm_val, probe.getEvalRank());

        normdiff += std::pow(perm_val - measurement[3],2);
        //std::cout << "loc" << location[0] << ":" << location[1] << " val " << val[0] << " vs " << measurement[2] << std::endl;

      }

      double density = -0.5 * normdiff * 10;

      return density;
    }
  }


  const int cells;

  std::shared_ptr<MultiIndex> index;
  std::shared_ptr<parcer::Communicator> comm;

  std::shared_ptr<Eigen::VectorXd> measurements_external;

  bool output = false;
  int cnt = 0;

  Problem problem;
  Eigen::VectorXd qoi;
  Eigen::VectorXd qoi_local;

  std::shared_ptr<GM> grid;
  std::shared_ptr<GV> gv;
  std::shared_ptr<FEM> fem;
  std::shared_ptr<GFS> gfs;
};


class MyMIComponentFactory : public ParallelizableMIComponentFactory {
public:


  MyMIComponentFactory (std::shared_ptr<Eigen::VectorXd> measurements_in)
   : measurements(measurements_in) {

  }

  void SetComm(std::shared_ptr<parcer::Communicator> const& comm) override {
    this->comm = comm;
  }

  virtual std::shared_ptr<MCMCProposal> Proposal (std::shared_ptr<MultiIndex> const& index, std::shared_ptr<AbstractSamplingProblem> const& samplingProblem) override {
    pt::ptree pt;
    pt.put("BlockIndex",0);

    auto mu = Eigen::VectorXd::Zero(paramSize(index));
    Eigen::MatrixXd cov = Eigen::MatrixXd::Identity(paramSize(index), paramSize(index));
    //cov *= 0.005;
    cov *= configuration.get<double>("prop_cov_factor");

    auto prior = std::make_shared<Gaussian>(mu, cov);


    return std::make_shared<MHProposal>(pt, samplingProblem, prior);
    //return std::make_shared<CrankNicolsonProposal>(pt, samplingProblem, prior);
  }

  virtual std::shared_ptr<MultiIndex> FinestIndex() override {
    auto index = std::make_shared<MultiIndex>(1);
    index->SetValue(0, LEVELS1-1);
    //index->SetValue(1, LEVELS2-1);
    return index;
  }

  virtual std::shared_ptr<MCMCProposal> CoarseProposal (std::shared_ptr<MultiIndex> const& index,
                                                        std::shared_ptr<AbstractSamplingProblem> const& coarseProblem,
                                                        std::shared_ptr<SingleChainMCMC> const& coarseChain) override {
    pt::ptree ptProposal;
    ptProposal.put("BlockIndex",0);
    int subsampling = 5;
    ptProposal.put("Subsampling", subsampling);

    //if (!multigrouprun)
      return std::make_shared<SubsamplingMIProposal>(ptProposal, coarseProblem, coarseChain);

    /*std::shared_ptr<MultiIndex> remoteIndex = MultiIndex::Copy(index);
    int maxCoeffId;
    int maxEntry = remoteIndex->GetVector().maxCoeff(&maxCoeffId);
    if (maxEntry == 0) {
      std::cerr << "whoopsie" << std::endl;
      exit(45);
    }
    // FIXME: Better way to get coarse index. Maybe pass in from outside?
    remoteIndex->SetValue(maxCoeffId, maxEntry - 1);

    return std::make_shared<RemoteMIProposal>(ptProposal, coarseProblem, global_comm, remoteIndex, PhonebookRank);*/
  }

  virtual std::shared_ptr<AbstractSamplingProblem> SamplingProblem (std::shared_ptr<MultiIndex> const& index) override {
    return std::make_shared<MySamplingProblem>(index, comm, measurements);
  }

  virtual std::shared_ptr<MIInterpolation> Interpolation (std::shared_ptr<MultiIndex> const& index) override {
    return std::make_shared<ConcatenatingInterpolation>(index);
  }

  virtual Eigen::VectorXd StartingPoint (std::shared_ptr<MultiIndex> const& index) override {
    //Eigen::VectorXd mu(2);
    //mu << 1.0, 2.0;
    return Eigen::VectorXd::Zero(paramSize(index));
    //return 0.005 * Eigen::VectorXd::Random(paramSize(index));
  }

private:
  std::shared_ptr<parcer::Communicator> comm;
  std::shared_ptr<Eigen::VectorXd> measurements;
};
