/** Parameter class for the stationary convection-diffusion equation of the following form:
 *
 * \f{align*}{
 *   \nabla\cdot(-A(x) \nabla u + b(x) u) + c(x)u &=& f \mbox{ in } \Omega,  \ \
 *                                              u &=& g \mbox{ on } \partial\Omega_D (Dirichlet)\ \
 *                (b(x,u) - A(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_N (Flux)\ \
 *                        -(A(x)\nabla u) \cdot n &=& o \mbox{ on } \partial\Omega_O (Outflow)
 * \f}
 * Note:
 *  - This formulation is valid for velocity fields which are non-divergence free.
 *  - Outflow boundary conditions should only be set on the outflow boundary
 *
 * The template parameters are:
 *  - GV a model of a GridView
 *  - RF numeric type to represent results
 */

#include <math.h>       /* sin */
#include <dune/randomfield/randomfield.hh>
#include "gridtraits.hh"
#include "spdlog/spdlog.h"

template<typename GV, typename RF>
class GenericEllipticProblem
{
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

public:

  typedef GridTraits<double,double,2> MyGridTraits;
  typedef Dune::RandomField::RandomFieldList<MyGridTraits> List;
  Dune::shared_ptr<List::SubRandomField> field;
  typedef List::SubRandomField::Traits RndFieldTraits;

private:
  std::shared_ptr<MultiIndex> index;

public:
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  //! tensor diffusion constant per cell? return false if you want more than one evaluation of A per cell.
  static constexpr bool permeabilityIsConstantPerCell()
  {
    return true;
  }


  GenericEllipticProblem(){}

  GenericEllipticProblem(std::shared_ptr<MultiIndex> index, std::shared_ptr<parcer::Communicator> comm)
   : index(index)
  {
    spdlog::info("Setting up GenericEllipticProblem for index " + index->ToString());

    {
      // Generate field
      Dune::ParameterTree config;
      Dune::ParameterTreeParser parserConfig;
      parserConfig.readINITree("randomfield2d.ini",config);

      if (config.get<bool>("generate", true)) {
        spdlog::info("generating field...");
        const int dim = 2;
        List randomFieldList(config, "", Dune::RandomField::DefaultLoadBalance<dim>(), comm->GetMPICommunicator());

        randomFieldList.generate(true);
        randomFieldList.writeToFile("field");
        spdlog::info("generated field");
      }
    }


    // Load field
    Dune::ParameterTree config;
    Dune::ParameterTreeParser parser;
    parser.readINITree("randomfield2d.ini",config);
    if (index->GetValue(0) == 0)
      config["grid.cells"] = "32 32";
    else
      config["grid.cells"] = "64 64";
    const int dim = 2;
    typedef GridTraits<double,double,2> GridTraits;
    List randomFieldList(config,"field", Dune::RandomField::DefaultLoadBalance<dim>(), comm->GetMPICommunicator());
    field = randomFieldList.get("perm");


  }

  void setParameters(std::vector<Eigen::VectorXd> const& parameters) {

    field->generate(42, [&](fftw_complex* extendedField, const auto* matrix, std::shared_ptr<RndFieldTraits> traits) {

      const std::array<unsigned int,RndFieldTraits::dim>& extendedCells = matrix->getExtendedCells();
      const std::array<unsigned int,RndFieldTraits::dim>& localExtendedCells = matrix->getLocalExtendedCells();
      const std::array<unsigned int,RndFieldTraits::dim>& localExtendedOffset = matrix->getLocalExtendedOffset();

      for (unsigned int index = 0; index < matrix->getLocalExtendedDomainSize(); index++)
      {
        extendedField[index][0] = 0.0;
        extendedField[index][1] = 0.0;
      }

      /*int c = 0;
      for (int l = 0; parameters.at(0).rows() > c; l++) {
        //for (int l = 0; l <= PARAM_SIZE[index->GetValue(0)]; l++) {
        for (int y = 0; y <= l; y++) {
          for (int x = 0; x <= l; x++) {

            if (y != l && x != l) // Only add new layers
              continue;

            int rx = x, ry = y;

            rx -= localExtendedOffset[0];
            ry -= localExtendedOffset[1];

            if (rx >= 0 && ry >= 0 && rx < localExtendedCells[0] && ry < localExtendedCells[1]) {

              std::array<unsigned int,RndFieldTraits::dim> indices = {(unsigned int)rx, (unsigned int)ry};
              unsigned int index = traits->indicesToIndex(indices, matrix->getLocalExtendedCells());

              //index =
              //std::cout << "writing #" << c << "\t at" << "\t" << x << "\t" << y << "\t" << rx << "\t" << ry << "\tidx " << index << "\t val:" << parameters.at(0)(c) << std::endl;
              assert (parameters.at(0).rows() > c);
              assert (index < matrix->getLocalExtendedDomainSize());
              assert (index >= 0);

              double lambda = std::sqrt(std::abs(matrix->fftTransformedMatrix[index][0]) / matrix->getExtendedDomainSize());
              if (x == 0 && y == 0)
                lambda *= 2;// * std::sqrt(2);

              if (extendedField[index][0] > 0 || extendedField[index][1] > 0)
                spdlog::error("Existing nonzero!!");
              extendedField[index][0] = lambda * parameters.at(0)(c);
              if (!(x == 0 && y == 0)) // Entry (0,0) leads to constant field both in real and imaginary part; therefore make it only one parameter
                extendedField[index][1] = lambda * parameters.at(0)(c+1);

              if (parameters.at(0)(c) > 0)
                spdlog::debug("Nonzero parameter {}", c);
              if (parameters.at(0)(c+1) > 0)
                spdlog::debug("Nonzero parameter {}", c+1);

            }

            if (x == 0 && y == 0)
              c+=1;
            else
              c+=2;

          }
        }

      }*/


      /*int c = 0;
      for (int l = 0; parameters.at(0).rows() > c; l++) {
        //for (int l = 0; l <= PARAM_SIZE[index->GetValue(0)]; l++) {
        for (int y = 0; y <= l; y++) {
          for (int x = -l; x <= l; x++) {

            if (std::abs(y) != l && std::abs(x) != l) // Only add new layers
              continue;

            int rx = x, ry = y;

            if (rx < 0)
              rx += extendedCells[0];
            if (ry < 0)
              ry += extendedCells[1];

            rx -= localExtendedOffset[0];
            ry -= localExtendedOffset[1];

            if (rx >= 0 && ry >= 0 && rx < localExtendedCells[0] && ry < localExtendedCells[1]) {

              std::array<unsigned int,RndFieldTraits::dim> indices = {(unsigned int)rx, (unsigned int)ry};
              unsigned int index = traits->indicesToIndex(indices, matrix->getLocalExtendedCells());

              //index =
              //std::cout << "writing #" << c << "\t at" << "\t" << x << "\t" << y << "\t" << rx << "\t" << ry << "\tidx " << index << "\t val:" << parameters.at(0)(c) << std::endl;
              assert (parameters.at(0).rows() > c);
              assert (index < matrix->getLocalExtendedDomainSize());
              assert (index >= 0);

              double lambda = std::sqrt(std::abs(matrix->fftTransformedMatrix[index][0]) / matrix->getExtendedDomainSize());

              extendedField[index][0] = lambda * parameters.at(0)(c);
              if (!(x == 0 && y == 0)) // Entry (0,0) leads to constant field both in real and imaginary part; therefore make it only one parameter
                extendedField[index][1] = lambda * parameters.at(0)(c+1);
            }

            if (x == 0 && y == 0)
              c+=1;
            else
              c+=2;

            //if (std::abs(y) != l)
            //  x += 2*l-1;
          }
        }
      }*/

      /*int c = 0;
      for (int l = 0; parameters.at(0).rows() > c; l++) {
      //for (int l = 0; l <= PARAM_SIZE[index->GetValue(0)]; l++) {
        for (int y = -l; y <= l; y++) {
          for (int x = -l; x <= l; x++) {

            if (std::abs(y) != l && std::abs(x) != l) // Only add new layers
              continue;

            int rx = x, ry = y;

            if (rx < 0)
              rx += extendedCells[0];
            if (ry < 0)
              ry += extendedCells[1];

            rx -= localExtendedOffset[0];
            ry -= localExtendedOffset[1];

            if (rx >= 0 && ry >= 0 && rx < localExtendedCells[0] && ry < localExtendedCells[1]) {

              std::array<unsigned int,RndFieldTraits::dim> indices = {(unsigned int)rx, (unsigned int)ry};
              unsigned int index = traits->indicesToIndex(indices, matrix->getLocalExtendedCells());

              //index =
              //std::cout << "writing #" << c << "\t at" << "\t" << x << "\t" << y << "\t" << rx << "\t" << ry << "\tidx " << index << "\t val:" << parameters.at(0)(c) << std::endl;
              assert (parameters.at(0).rows() > c);
              assert (index < matrix->getLocalExtendedDomainSize());
              assert (index >= 0);

              double lambda = std::sqrt(std::abs(matrix->fftTransformedMatrix[index][0]) / matrix->getExtendedDomainSize());

              extendedField[index][0] = lambda * parameters.at(0)(c);
              if (!(x == 0 && y == 0)) // Entry (0,0) leads to constant field both in real and imaginary part; therefore make it only one parameter
                extendedField[index][1] = lambda * parameters.at(0)(c+1);
            }

            if (x == 0 && y == 0)
              c+=1;
            else
              c+=2;

            //if (std::abs(y) != l)
            //  x += 2*l-1;
          }
        }
      }*/

      int c = 0;
      for (int l = 0; parameters.at(0).rows() > c; l++) {
        //for (int l = 0; l <= PARAM_SIZE[index->GetValue(0)]; l++) {
        for (int y = 0; y <= l; y++) {
          for (int x = -l; x <= l; x++) {

            if (std::abs(y) != l && std::abs(x) != l) // Only add new layers
              continue;
            if (y == 0 && x < 0)
              continue;

            {
              int rx = x, ry = y;

              if (rx < 0)
                rx += extendedCells[0];
              if (ry < 0)
                ry += extendedCells[1];

              rx -= localExtendedOffset[0];
              ry -= localExtendedOffset[1];

              if (rx >= 0 && ry >= 0 && rx < localExtendedCells[0] && ry < localExtendedCells[1]) {

                std::array<unsigned int,RndFieldTraits::dim> indices = {(unsigned int)rx, (unsigned int)ry};
                unsigned int index = traits->indicesToIndex(indices, matrix->getLocalExtendedCells());

                //index =
                //std::cout << "writing #" << c << "\t at" << "\t" << x << "\t" << y << "\t" << rx << "\t" << ry << "\tidx " << index << "\t val:" << parameters.at(0)(c) << std::endl;
                assert (parameters.at(0).rows() > c);
                assert (index < matrix->getLocalExtendedDomainSize());
                assert (index >= 0);

                double lambda = std::sqrt(std::abs(matrix->fftTransformedMatrix[index][0]) / matrix->getExtendedDomainSize() / 2);

                extendedField[index][0] = lambda * parameters.at(0)(c);
                extendedField[index][1] = -lambda * parameters.at(0)(c);
              }
            }

            {
              int rx = -x, ry = -y;

              if (rx < 0)
                rx += extendedCells[0];
              if (ry < 0)
                ry += extendedCells[1];

              rx -= localExtendedOffset[0];
              ry -= localExtendedOffset[1];

              if (rx >= 0 && ry >= 0 && rx < localExtendedCells[0] && ry < localExtendedCells[1]) {

                std::array<unsigned int,RndFieldTraits::dim> indices = {(unsigned int)rx, (unsigned int)ry};
                unsigned int index = traits->indicesToIndex(indices, matrix->getLocalExtendedCells());

                //index =
                //std::cout << "writing #" << c << "\t at" << "\t" << x << "\t" << y << "\t" << rx << "\t" << ry << "\tidx " << index << "\t val:" << parameters.at(0)(c) << std::endl;
                assert (parameters.at(0).rows() > c);
                assert (index < matrix->getLocalExtendedDomainSize());
                assert (index >= 0);

                double lambda = std::sqrt(std::abs(matrix->fftTransformedMatrix[index][0]) / matrix->getExtendedDomainSize() / 2);

                extendedField[index][0] = lambda * parameters.at(0)(c);
                extendedField[index][1] = -lambda * parameters.at(0)(c);
              }
            }

            c++;

          }
        }
      }
      if (parameters.at(0).rows() != c) {
        spdlog::error("Parameter dimension mismatch!");
      }

      /*uint nonzeros = 0;
      for (unsigned int index = 0; index < matrix->getLocalExtendedDomainSize(); index++)
      {
        if (extendedField[index][0] != .0 || extendedField[index][1] != .0)
          nonzeros++;
      }
      if (nonzeros > 2)
        spdlog::error("More than two nonzero!!");*/

    }, true);

  }

  void perm (const typename Traits::DomainType& xglobal, double& permeability) const {
    if (_is_reference && configuration.get<bool>("artificial_reference")) { // FIXME: This is switched off!
      //std::cout << "is reference" << std::endl;
      double x = xglobal[0];
      double y = xglobal[1];

      permeability = .0;

      permeability += std::exp( std::max(0.0, 1.0 - std::pow(x-0.3,2)*10.0 - std::pow(y-0.3,2)*10.0)) - 1.0;
      permeability += std::exp( std::max(0.0, 1.0 - std::pow(x-0.7,2)*15.0 - std::pow(y-0.7,2)*15.0)) - 1.0;
      permeability += std::exp( std::max(0.0, 1.0 - std::pow(x-0.8,2)*30.0 - std::pow(y-0.2,2)*30.0)) - 1.0;
      permeability += std::exp( std::max(0.0, 1.0 - std::pow(x-0.2,2)*50.0 - std::pow(y-0.8,2)*50.0)) - 1.0;

      permeability += 1e-1;
    } else {
      Dune::FieldVector<double, 1> field_val;
      field->evaluate(xglobal,field_val);
      permeability = std::exp(field_val[0]);
    }
    permeability *= permeability * permeability; // Increase contrast a bit for more interesting solutions
  }

  //! tensor diffusion coefficient
  typename Traits::PermTensorType
  A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType xglobal = e.geometry().center(); //.global(x); // TODO: Center

    double permeability;
    perm(xglobal, permeability);

    typename Traits::PermTensorType I;
    I[0][0]=permeability;
    I[0][1]=0;
    I[1][0]=0;
    I[1][1]=permeability;

    return I;
  }

public:
  bool _is_reference = false;

  void setReference (bool isReference) {
    _is_reference = isReference;
  }

  //! velocity field
  typename Traits::RangeType
  b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType v(0.0);
    return v;
  }

  //! sink term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! boundary condition type function
  /* return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet for Dirichlet boundary conditions
   * return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann for flux boundary conditions
   * return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Outflow for outflow boundary conditions
   */
  BCType
  bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    typename Traits::DomainType xglobal = is.geometry().global(x);
    if (!(xglobal[0]<1E-6 || xglobal[0]>1.0-1E-6))
      return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
    else
      return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
    /*if (!(xglobal[1]<1E-6 || xglobal[1]>1.0-1E-6))
      return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
    else
      return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;*/
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType xglobal = e.geometry().global(x);
    if (xglobal[0] > 1.0-1E-6)
      return 1.0;
    else
      return 0.0;
    /*if (xglobal[1] > 1.0-1E-6)
      return 1.0;
    else
      return 0.0;*/
  }

  //! flux boundary condition
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! outflow boundary condition
  typename Traits::RangeFieldType
  o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }
};
