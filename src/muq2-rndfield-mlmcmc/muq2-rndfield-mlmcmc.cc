#include "config.h"

// PDELab

#include <dune/pdelab/boilerplate/pdelab.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionadapter.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/pdelab/common/functionutilities.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
//#include <dune/pdelab/finiteelementmap/monomfem.hh>
#include <dune/pdelab/backend/istl/seqistlsolverbackend.hh>



// Config

#include <dune/common/parametertree.hh>
Dune::ParameterTree configuration;


// MUQ

#include "MUQ/SamplingAlgorithms/GreedyMLMCMC.h"
#include "MUQ/SamplingAlgorithms/MIMCMC.h"
#include "MUQ/SamplingAlgorithms/SLMCMC.h"

#include "MUQ/Modeling/Distributions/Gaussian.h"
#include "MUQ/Modeling/Distributions/Density.h"

#include "MUQ/SamplingAlgorithms/MHKernel.h"
#include "MUQ/SamplingAlgorithms/MHProposal.h"
#include "MUQ/SamplingAlgorithms/DummyKernel.h"
#include "MUQ/SamplingAlgorithms/CrankNicolsonProposal.h"
#include "MUQ/SamplingAlgorithms/SamplingProblem.h"
#include "MUQ/SamplingAlgorithms/SubsamplingMIProposal.h"

#include "MUQ/SamplingAlgorithms/MIComponentFactory.h"

#include <boost/property_tree/ptree.hpp>

namespace pt = boost::property_tree;
using namespace muq::Modeling;
using namespace muq::SamplingAlgorithms;
using namespace muq::Utilities;

#include "rndfield-mimcmc-problem.hh"

#include "MUQ/Utilities/AnyHelpers.h"

#include "parcer/Communicator.h"


#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

#include <dune/common/timer.hh>


#include "MUQ/SamplingAlgorithms/Phonebook.h"
#include "MUQ/SamplingAlgorithms/ParallelMIMCMCWorker.h"
#include "MUQ/SamplingAlgorithms/ParallelFixedSamplesMIMCMC.h"
#include "problem.hh"


class CustomStaticLoadBalancer : public StaticLoadBalancer {
public:
  void setup(std::shared_ptr<ParallelizableMIComponentFactory> componentFactory, uint availableRanks) override {
    ranks_remaining = availableRanks;
    std::cout << "Have " << availableRanks << " ranks" << std::endl;
    auto indices = MultiIndexFactory::CreateFullTensor(componentFactory->FinestIndex()->GetVector());
    models_remaining = indices->Size();
  }

  int numCollectors(std::shared_ptr<MultiIndex> modelIndex) override {
    ranks_remaining--;
    return 1;
  }
  WorkerAssignment numWorkers(std::shared_ptr<MultiIndex> modelIndex) override {
    WorkerAssignment assignment;

    assignment.numWorkersPerGroup = 1;
    if (modelIndex->GetValue(0) == 0)
      assignment.numGroups = ranks_remaining / 4;
    else
      assignment.numGroups = ranks_remaining;
    std::cout << "Of " << ranks_remaining << ", assigning " << assignment.numGroups * assignment.numWorkersPerGroup << " to " << *modelIndex << std::endl;

    models_remaining--;
    ranks_remaining -= assignment.numGroups * assignment.numWorkersPerGroup;

    return assignment;
  }
private:
  uint ranks_remaining;
  uint models_remaining;

};




int main(int argc, char** argv){

  spdlog::set_level(spdlog::level::err);

  Dune::ParameterTreeParser parser;
  parser.readINITree("config.ini",configuration);
  parser.readOptions(argc, argv, configuration);

  pt::ptree pt;
  //pt.put("NumSamples", configuration.get<int>("NumSamples")); // number of samples for single level
  //pt.put("NumInitialSamples", configuration.get<int>("NumInitialSamples")); // number of initial samples for greedy MLMCMC
  //pt.put("GreedyTargetVariance", 10);
  pt.put("MCMC.BurnIn", 100); // number of samples for single level
  pt.put("MLMCMC.Subsampling", 100);
  pt.put("MLMCMC.Scheduling", "true");
  pt.put("NumSamples_0", 1e5);
  pt.put("NumSamples_1", 1e3);
  pt.put("NumSamples_2", 1e1);


  Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
  auto comm = std::make_shared<parcer::Communicator>(MPI_COMM_WORLD);

  // Name trace according to current time stamp
  std::time_t result = std::time(nullptr);
  std::string timestamp = std::asctime(std::localtime(&result));
  comm->Bcast(timestamp, 0);
  auto tracer = std::make_shared<OTF2Tracer>("trace", timestamp);

  // Compute artificial measurements
  Eigen::VectorXd measurements;
  if (comm->GetRank() == 0) {

    auto measurementFactory = std::make_shared<MyMIComponentFactory>(nullptr);
    measurementFactory->SetComm(std::make_shared<parcer::Communicator>(MPI_COMM_SELF));

    auto measurementProblem = std::dynamic_pointer_cast<MySamplingProblem>(measurementFactory->SamplingProblem(measurementFactory->FinestIndex()));
    measurements = *measurementProblem->GetMeasurements();
  }

  //std::cout << "Broadcast measurements" << std::endl;
  comm->Bcast(measurements, 0);


  // Run parallel MLMCMC
  auto componentFactory = std::make_shared<MyMIComponentFactory>(std::make_shared<Eigen::VectorXd>(measurements));

  StaticLoadBalancingMIMCMC parallelMIMCMC (pt, componentFactory, std::make_shared<CustomStaticLoadBalancer>(), comm, tracer);
  if (comm->GetRank() == 0) {
    parallelMIMCMC.Run();
    Eigen::VectorXd meanQOI = parallelMIMCMC.MeanQOI();

    componentFactory->SetComm(std::make_shared<parcer::Communicator>(MPI_COMM_SELF));
    std::shared_ptr<MySamplingProblem> finestProblem = std::dynamic_pointer_cast<MySamplingProblem>(componentFactory->SamplingProblem(componentFactory->FinestIndex()));
    finestProblem->writeVTKfromQOI(meanQOI, "ml_mean");
  }
  parallelMIMCMC.WriteToFile("muq2-rndfield-mlmcmc.h5");
  parallelMIMCMC.Finalize();
  tracer->write();

  return 0;
}
