#include "config.h"

// PDELab

#include <dune/pdelab/boilerplate/pdelab.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionadapter.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/pdelab/common/functionutilities.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
//#include <dune/pdelab/finiteelementmap/monomfem.hh>
#include <dune/pdelab/backend/istl/seqistlsolverbackend.hh>



// Config

#include <dune/common/parametertree.hh>
Dune::ParameterTree configuration;


// MUQ

#include "MUQ/SamplingAlgorithms/GreedyMLMCMC.h"
#include "MUQ/SamplingAlgorithms/MIMCMC.h"
#include "MUQ/SamplingAlgorithms/SLMCMC.h"

#include "MUQ/Modeling/Distributions/Gaussian.h"
#include "MUQ/Modeling/Distributions/Density.h"

#include "MUQ/SamplingAlgorithms/MHKernel.h"
#include "MUQ/SamplingAlgorithms/MHProposal.h"
#include "MUQ/SamplingAlgorithms/DummyKernel.h"
#include "MUQ/SamplingAlgorithms/CrankNicolsonProposal.h"
#include "MUQ/SamplingAlgorithms/SamplingProblem.h"
#include "MUQ/SamplingAlgorithms/SubsamplingMIProposal.h"

#include "MUQ/SamplingAlgorithms/MIComponentFactory.h"

#include <boost/property_tree/ptree.hpp>

namespace pt = boost::property_tree;
using namespace muq::Modeling;
using namespace muq::SamplingAlgorithms;
using namespace muq::Utilities;

#include "rndfield-mimcmc-problem.hh"

#include "MUQ/Utilities/AnyHelpers.h"

#include "parcer/Communicator.h"


#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

#include <dune/common/timer.hh>


#include "MUQ/SamplingAlgorithms/Phonebook.h"
#include "MUQ/SamplingAlgorithms/ParallelMIComponentFactory.h"
#include "problem.hh"


int main(int argc, char** argv){

  spdlog::set_level(spdlog::level::debug);

  Dune::ParameterTreeParser parser;
  parser.readINITree("config.ini",configuration);
  parser.readOptions(argc, argv, configuration);


  Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

  auto comm = std::make_shared<parcer::Communicator>();

  if (comm->GetSize() > 1) {
    if (comm->GetRank() == 0)
      std::cout << "Intended to run sequentially" << std::endl;
    exit(-1);
  }

  Eigen::VectorXd measurements;
  if (comm->GetRank() == 0) {

    auto comm_self = std::make_shared<parcer::Communicator>(MPI_COMM_SELF);
    auto measurementFactory = std::make_shared<MyMIComponentFactory>(nullptr);
    measurementFactory->SetComm(comm_self);

    auto measurementProblem = std::dynamic_pointer_cast<MySamplingProblem>(measurementFactory->SamplingProblem(measurementFactory->FinestIndex()));
    std::cout << "Rank 0 computed measurements" << std::endl;
    measurements = *measurementProblem->GetMeasurements();
  }
  std::cout << "Broadcast measurements" << std::endl;
  comm->Bcast(measurements, 0);
  std::cout << measurements << std::endl;




  auto comm_self = std::make_shared<parcer::Communicator>(MPI_COMM_SELF);
  auto componentFactory = std::make_shared<MyMIComponentFactory>(std::make_shared<Eigen::VectorXd>(measurements));
  componentFactory->SetComm(comm_self);


  spdlog::info("Plotting modes");

  auto finemodelindex = std::make_shared<MultiIndex>(1, 1);
  auto coarsemodelindex = std::make_shared<MultiIndex>(1, 0);
  //for (int modeID = 0; modeID < std::min(paramSize(coarsemodelindex), 40); ++modeID) {
  for (int modeID = 0; modeID < paramSize(coarsemodelindex); ++modeID) {
    spdlog::debug("Plotting mode {}", modeID);
    Eigen::VectorXd testparam_fine = Eigen::VectorXd::Zero(paramSize(finemodelindex));
    testparam_fine(modeID) = 1.0;
    std::stringstream ss;
    ss << std::setw(4) << std::setfill('0') << modeID;
    {
      auto modelindex = coarsemodelindex;
      spdlog::debug("Set up coarse sampling problem {}", modeID);
      std::shared_ptr<MySamplingProblem> testProblem = std::dynamic_pointer_cast<MySamplingProblem>(componentFactory->SamplingProblem(modelindex));
      Eigen::VectorXd testparam = testparam_fine.head(paramSize(modelindex));
      double density;
      spdlog::debug("Testing coarse mode {}", modeID);
      testProblem->test(std::make_shared<SamplingState>(testparam), "refine_test_lvl0_mode" + ss.str(), density);
    }
    {
      auto modelindex = finemodelindex;
      spdlog::debug("Set up fine sampling problem {}", modeID);
      std::shared_ptr<MySamplingProblem> testProblem = std::dynamic_pointer_cast<MySamplingProblem>(componentFactory->SamplingProblem(modelindex));
      Eigen::VectorXd testparam = testparam_fine;
      for (int i = paramSize(coarsemodelindex); i < paramSize(finemodelindex); i++)
        testparam(i) = 0.0;
      spdlog::debug("Testing fine mode {}", modeID);
      double density;
      testProblem->test(std::make_shared<SamplingState>(testparam), "refine_test_lvl1_mode" + ss.str(), density);
    }
  }


}
