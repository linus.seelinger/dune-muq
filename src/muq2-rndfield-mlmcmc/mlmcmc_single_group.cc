#include "config.h"

// PDELab

#include <dune/pdelab/boilerplate/pdelab.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionadapter.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/pdelab/common/functionutilities.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
//#include <dune/pdelab/finiteelementmap/monomfem.hh>
#include <dune/pdelab/backend/istl/seqistlsolverbackend.hh>



// Config

#include <dune/common/parametertree.hh>
Dune::ParameterTree configuration;


// MUQ

#include "MUQ/SamplingAlgorithms/GreedyMLMCMC.h"
#include "MUQ/SamplingAlgorithms/MIMCMC.h"
#include "MUQ/SamplingAlgorithms/SLMCMC.h"

#include "MUQ/Modeling/Distributions/Gaussian.h"
#include "MUQ/Modeling/Distributions/Density.h"

#include "MUQ/SamplingAlgorithms/MHKernel.h"
#include "MUQ/SamplingAlgorithms/MHProposal.h"
#include "MUQ/SamplingAlgorithms/DummyKernel.h"
#include "MUQ/SamplingAlgorithms/CrankNicolsonProposal.h"
#include "MUQ/SamplingAlgorithms/SamplingProblem.h"
#include "MUQ/SamplingAlgorithms/SubsamplingMIProposal.h"

#include "MUQ/SamplingAlgorithms/MIComponentFactory.h"

#include <boost/property_tree/ptree.hpp>

namespace pt = boost::property_tree;
using namespace muq::Modeling;
using namespace muq::SamplingAlgorithms;
using namespace muq::Utilities;

#include "rndfield-mimcmc-problem.hh"

#include "MUQ/Utilities/AnyHelpers.h"

#include "parcer/Communicator.h"


#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

#include <dune/common/timer.hh>


#include "MUQ/SamplingAlgorithms/Phonebook.h"
#include "MUQ/SamplingAlgorithms/ParallelMIComponentFactory.h"
#include "problem.hh"



int main(int argc, char** argv){

  Dune::ParameterTreeParser parser;
  parser.readINITree("config.ini",configuration);
  parser.readOptions(argc, argv, configuration);


  Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

  auto comm = std::make_shared<parcer::Communicator>();

  std::cout << "Rank: " <<  comm->GetRank() << std::endl;


  // Compute artificial measurements
  Eigen::VectorXd measurements;
  if (comm->GetRank() == 0) {

    auto comm_self = std::make_shared<parcer::Communicator>(MPI_COMM_SELF);
    auto measurementFactory = std::make_shared<MyMIComponentFactory>(nullptr);
    measurementFactory->SetComm(comm_self);

    auto measurementProblem = std::dynamic_pointer_cast<MySamplingProblem>(measurementFactory->SamplingProblem(measurementFactory->FinestIndex()));
    std::cout << "Rank 0 computed measurements" << std::endl;
    measurements = *measurementProblem->GetMeasurements();
  }
  std::cout << "Broadcast measurements" << std::endl;
  comm->Bcast(measurements, 0);
  std::cout << measurements << std::endl;

  auto componentFactory = std::make_shared<MyMIComponentFactory>(std::make_shared<Eigen::VectorXd>(measurements));
  componentFactory->SetComm(comm);
  auto parallelComponentFactory = std::make_shared<ParallelMIComponentFactory>(comm, comm, componentFactory);

  if (comm->GetRank() == 0) {

    pt::ptree pt;

    pt.put("NumSamples", configuration.get<int>("NumSamples")); // number of samples for single level
    pt.put("NumInitialSamples", configuration.get<int>("NumInitialSamples")); // number of initial samples for greedy MLMCMC
    pt.put("GreedyTargetVariance", configuration.get<double>("GreedyTargetVariance"));//0.2); // estimator variance to be achieved by greedy algorithm
    //pt.put("BurnIn", 25); // burnin for single level
    auto comm_self = std::make_shared<parcer::Communicator>(MPI_COMM_SELF);
    auto sequentialFactory = std::make_shared<MyMIComponentFactory>(std::make_shared<Eigen::VectorXd>(measurements));
    sequentialFactory->SetComm(comm_self);

    std::shared_ptr<MySamplingProblem> finestProblem = std::dynamic_pointer_cast<MySamplingProblem>(sequentialFactory->SamplingProblem(sequentialFactory->FinestIndex()));


    std::cout << std::endl << "*************** single chain reference" << std::endl << std::endl;


    std::cout << "Setting up SLMCMC" << std::endl;
    SLMCMC slmcmc (pt, sequentialFactory);
    std::cout << "SLMCMC set up" << std::endl;
    slmcmc.Run();

    std::cout << std::endl << "*************** ref result check" << std::endl << std::endl;

    //finestProblem->test(std::make_shared<SamplingState>(slmcmc.MeanParameter()), density);
    finestProblem->writeVTKfromQOI(slmcmc.MeanQOI(), "sl_mean_QOI");



    std::cout << std::endl << "*************** greedy multillevel chain" << std::endl << std::endl;

    GreedyMLMCMC greedymlmcmc (pt, parallelComponentFactory);
    std::cout << "GreedyMLMCMC run started" << std::endl;
    greedymlmcmc.Run();

    std::cout << "GreedyMLMCMC run finished" << std::endl;

    finestProblem->writeVTKfromQOI(greedymlmcmc.MeanQOI(), "ml_mean");


    for (int level = 0; level <= parallelComponentFactory->FinestIndex()->GetValue(0); level++) {
      auto box = greedymlmcmc.GetBox(level);
      std::shared_ptr<SampleCollection> qois = box->FinestChain()->GetQOIs();
      std::shared_ptr<SampleCollection> states = box->FinestChain()->GetSamples();
      finestProblem->writeVTKfromQOI(box->MeanQOI(), "lvl_" + std::to_string(level) + "_mean");

      Eigen::VectorXd rolling_mean = Eigen::VectorXd::Zero(qois->at(0)->state[0].rows());
      for (int i = 0; i < qois->size(); i++) {
        auto qoi = qois->at(i);
        auto state = states->at(i);
        finestProblem->writeVTKfromQOI(qoi->state[0], "lvl_" + std::to_string(level) + "_s" + std::to_string(i));

        if(state->HasMeta("coarseSample")) {
          std::shared_ptr<SamplingState> coarseState = AnyCast(state->meta["coarseSample"]);
          std::shared_ptr<SamplingState> coarseQOI = AnyCast(coarseState->meta["QOI"]);
          finestProblem->writeVTKfromQOI(coarseQOI->state[0], "lvl_" + std::to_string(level) + "_c_s" + std::to_string(i));
          rolling_mean -= coarseQOI->state[0];// / (double)qois->size();
        }

        rolling_mean += qoi->state[0];// / (double)qois->size();
        finestProblem->writeVTKfromQOI(rolling_mean / (double)(i+1), "lvl_" + std::to_string(level) + "_rolling_mean" + std::to_string(i));

      }
    }

    /*for (int i = 0; i < 2; i++) {
      std::shared_ptr<SingleChainMCMC> finestChain = greedymlmcmc.GetBox(i)->FinestChain();
      finestProblem.writeVTKfromQOI(finestChain->GetQOIs()->Mean(), "main_QOI_l" + std::to_string(i));
      finestProblem.writeVTKfromQOI(greedymlmcmc.GetBox(i)->MeanQOI(), "main_QOI_diff_l" + std::to_string(i));
      std::shared_ptr<SampleCollection> samples = finestChain->GetSamples();

      double logTargetMax = -1;
      std::shared_ptr<SamplingState> stateMax = nullptr;
      for (int s = 0; s < samples->size(); s++) {
        double logTarget = AnyCast(samples->at(s)->meta["LogTarget"]);
        std::cout << logTarget << std::endl;
        if (logTarget > logTargetMax || s == 0) {
          logTargetMax = logTarget;
          stateMax = samples->at(s);
        }
      }

      if (stateMax == nullptr)
        std::cout << "au." << std::endl;

      if (!stateMax->HasMeta("QOI"))
        std::cout << "nope" << std::endl;
      else {
        std::shared_ptr<SamplingState> qoi = AnyCast(stateMax->meta["QOI"]);
        finestProblem.writeVTKfromQOI(qoi->state[0], "numMAP_QOI_l" + std::to_string(i));
      }
    }*/

    //finestProblem.test(std::make_shared<SamplingState>(greedymlmcmc.meanQOI()), density);

  }

  parallelComponentFactory->finalize();

  return 0;
}
