#include "MUQ/SamplingAlgorithms/ParallelizableMIComponentFactory.h"
#include<dune/modelling/forwardadjointmodel.hh>
#include <fstream>
#include "MUQ/SamplingAlgorithms/ConcatenatingInterpolation.h"

#include <dune/pdelab/function/callableadapter.hh>

//int PARAM_DIM = 5*5*2;
//int PARAM_DIM = 15*15*2;
const int QOI_GRID_SIZE = 16;

int ParamDim(std::shared_ptr<MultiIndex> index) {
  /*if (index->GetValue(0) == 0)
    return 15*15*2;
  if (index->GetValue(0) == 1)
    return 21*21*2;
  spdlog::error("unknown model requested in ParamDim");
  exit(-1);*/
  return 15;
}

class MySamplingProblem : public AbstractSamplingProblem {
public:
  MySamplingProblem(Dune::MPIHelper& mpi_helper, std::shared_ptr<MultiIndex> index, std::shared_ptr<parcer::Communicator> comm, std::string name_prefix)
   : AbstractSamplingProblem(Eigen::VectorXi::Constant(1,ParamDim(index)), Eigen::VectorXi::Constant(1,QOI_GRID_SIZE*QOI_GRID_SIZE)),
     mpi_helper_(mpi_helper),
     comm_(comm), name_prefix_(name_prefix) {

    Dune::ParameterTreeParser parser;
    parser.readINITree("inversion.ini",config);
    if (index->GetValue(0) == 0)
      config["grid.cells"] = "32 64";
    else if (index->GetValue(0) == 1)
      config["grid.cells"] = "64 128";
      //config["grid.cells"] = "64 128";
    else
      exit(-1);


    traits = std::make_shared<MyInversionTraits>(comm_->GetMPICommunicator(),config);
    //traits = std::make_shared<MyInversionTraits>(mpi_helper.getCommunicator(),config);
    //traits = std::make_shared<MyInversionTraits>(mpi_helper_,config);

    if (traits->rank() == 0)
    {
      std::cout << "------------------" << std::endl;
      std::cout << "filling model list" << std::endl;
      std::cout << "------------------" << std::endl;
    }


    modelList = std::make_shared<ModelList>(*traits);
    modelList->template add<FlowModel>("head");
    modelList->template add<TransportModel,FlowModel>("tracer",{"head"});
    if (traits->rank() == 0) modelList->report(std::cout);

    Dune::ParameterTree measConfig = config;
    measConfig["randomField.types"] = config["measurements.types"];
//    measConfig["randomField.active"] = config["measurements.active"];
    measConfig["grid.cells"]        = config["measurements.cells"];
    measurementList = std::make_shared<MeasurementList>(measConfig,measConfig.get<std::string>("measurements.referenceName",""),Dune::RandomField::DefaultLoadBalance<dim>(),comm_->GetMPICommunicator());

    if (traits->rank() == 0)
    {
      std::cout << "-------------------" << std::endl;
      std::cout << "reading input field" << std::endl;
      std::cout << "-------------------" << std::endl;
    }



    Dune::FieldVector<double,dim> L(1.0);
    auto L_ini = config.get<std::vector<double>>("grid.extensions");
    L[0] = L_ini[0];
    L[1] = L_ini[1];
    std::array<int,dim> N;
    std::fill(N.begin(), N.end(), QOI_GRID_SIZE);
    typedef Dune::YaspFixedSizePartitioner<dim> YP;
    std::array<int,dim> yasppartitions = {comm->GetSize(), 1};
    //auto yp = new YP(yasppartitions);
    int overlap = 0;

    std::bitset<dim> B(false);
    grid = std::make_shared<GM>(L,N,B,overlap,comm->GetMPICommunicator());//, yp);
    grid->loadBalance();

    gv = std::make_shared<GV>(grid->levelGridView(grid->maxLevel()));

    fem = std::make_shared<FEM>(Dune::GeometryType(Dune::GeometryType::cube, dim));

    gfs = std::make_shared<GFS>(*gv,*fem);
    gfs->update();

    qoi = Eigen::VectorXd::Zero(QOI_GRID_SIZE*QOI_GRID_SIZE);

  }

  virtual ~MySamplingProblem() {
    modelList = nullptr; //Need this because modelList destructor assumes *lists to exist
    measurementList = nullptr;
    parameterList = nullptr;
    traits = nullptr;
  }

  int eval_cnt = 0;

  static constexpr int dim = 2;

  using DF = double;
  using RF = double;

  using MyGridTraits      = GridTraits<DF,RF,2>;
  using MyInversionTraits = InversionTraits<MyGridTraits>;

  using FlowModelType = ModelType::Groundwater;
  using FormulationType= Dune::Modelling::Formulation::Transient;

  using ModelList = Dune::Modelling::ForwardModelList<MyInversionTraits>;
  using FlowModel = Dune::Modelling::ForwardModel<MyInversionTraits,FlowModelType,FormulationType>;
  using TransportModel = Dune::Modelling::ForwardModel<MyInversionTraits,ModelType::Transport,FormulationType>;
  using ParameterList   = typename MyInversionTraits::ParameterList;
  using MeasurementList = typename MyInversionTraits::MeasurementList;


  //using GFS = typename Dune::Modelling::EquationTraits<MyInversionTraits,FlowModelType,Dune::Modelling::Direction::Forward>::GridFunctionSpace;
  typedef Dune::YaspGrid<dim> GM;
  typedef typename GM::LevelGridView GV;
  //using GFS = MyInversionTraits::ParamGFS;
  //using GridTraits = MyGridTraits;
  using DomainField = typename MyGridTraits::DomainField;
  using RangeField  = typename MyGridTraits::RangeField;
  using FEM = Dune::PDELab::P0LocalFiniteElementMap<DomainField,RangeField,dim>;
  typedef Dune::PDELab::NoConstraints CON;
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  using V = Dune::PDELab::Backend::Vector<GFS,RF>;
  std::shared_ptr<GM> grid;
  std::shared_ptr<GV> gv;
  std::shared_ptr<FEM> fem;
  std::shared_ptr<GFS> gfs;


  Dune::ParameterTree config;
  std::shared_ptr<ParameterList> parameterList;
  std::shared_ptr<MeasurementList> measurementList;
  std::shared_ptr<ModelList> modelList;
  std::shared_ptr<MyInversionTraits> traits;


  static std::string toString(const Eigen::MatrixXd& mat){
    std::stringstream ss;
    ss << mat;
    return ss.str();
  }

  virtual double LogDensity(unsigned int const t, std::shared_ptr<SamplingState> const& state, AbstractSamplingProblem::SampleType type) override {
    eval_cnt++;
    lastState = state;

    Dune::ParameterTree paramConfig = config;
    paramConfig["randomField.types"] = config["parameters.types"];
    paramConfig["randomField.active"] = config["parameters.active"];
    paramConfig["grid.cells"]        = config["parameters.cells"];
    //paramConfig["grid.cells"]        = config["grid.cells"]; // Use same dims as FE grid

    Dune::ParameterTree logCondConfig;
    Dune::ParameterTreeParser parser2;
    parser2.readINITree("logCond.field",logCondConfig);

    int param_cnt = 0;
    std::cout << "Param: " << state->state.at(0) << std::endl;
    for (int blockid = 0; blockid < 3; blockid++) {
      std::string entry = "block" + std::to_string(blockid) + ".mean";

      std::string str = "";
      for (int i = 0; i < 5; i++) {
        str += std::to_string(state->state.at(0)(param_cnt)) + " ";
        param_cnt++;
      }
      //str += "5";
      std::replace(str.begin(), str.end(), ',', '.');
      logCondConfig[entry] = str;
      std::cout << "String: " << str << std::endl;
    }

    parameterList = std::make_shared<ParameterList>(paramConfig,paramConfig.get<std::string>("paramField.inputName",""),Dune::RandomField::DefaultLoadBalance<dim>(),comm_->GetMPICommunicator(),logCondConfig);
    auto field = parameterList->get("logCond");
    //field->generate_trend(true);
    parameterList->writeToVTK(name_prefix_ + "_param_" + std::to_string(eval_cnt), "param", traits->grid().leafGridView());


    /*auto parameterGridFunction = Dune::PDELab::makeGridFunctionFromCallable (traits->grid().leafGridView(), [&](const auto& elem, const auto& x){
      Dune::FieldVector<double, 1> field_val;
      field->evaluate(elem, x, field_val);
      return (RF)field_val[0];
    });
    V paramfield(traits->gfs(),0.0);*
    Dune::PDELab::interpolate(parameterGridFunction,traits->gfs(),paramfield);*/

    /*Dune::FieldVector<double, 1> field_val;
    Dune::FieldVector<double, 2> x;
    field_val = field->evaluate(x);*/
    auto parameterGridFunction = Dune::PDELab::makeGridFunctionFromCallable (*gv, [&](const auto& x){
      Dune::FieldVector<double, 1> field_val;
      field->evaluate(x, field_val);
      return (RF)field_val[0];
    });
    // make a degree of freedom vector
    V u(*gfs,0.0);
    // interpolate from a given function
    Dune::PDELab::interpolate(parameterGridFunction,*gfs,u);
    using Dune::PDELab::Backend::native;
    for (int i = 0; i < u.N(); i++)
      qoi(i) = native(u)[i];

    param_cnt = 0;
    for (int blockid = 0; blockid < 3; blockid++) {
      double centerx = state->state.at(0)(param_cnt+0);
      double centery = state->state.at(0)(param_cnt+1);
      double width = state->state.at(0)(param_cnt+2);
      double height = state->state.at(0)(param_cnt+3);
      double perm = state->state.at(0)(param_cnt+4);

      if (centerx - width / 2.0 < 0 || centery - height / 2.0 < 0.2
        || centerx + width / 2.0 > 0.29 || centery + height / 2.0 > 0.5
        || width < 0.01 || height < 0.01 || perm < -5.0 || perm > -1.0) {

{
  std::ofstream outfile;
  if(eval_cnt > 1)
    outfile.open(name_prefix_ + "_logdens.txt", std::ios_base::app);
  else
    outfile.open(name_prefix_ + "_logdens.txt", std::ios_base::out | std::ios_base::trunc);
/*  outfile << "state: " << toString(state->state.at(0)) << std::endl;
  outfile << "ini0: " << logCondConfig["block0.mean"] << std::endl;
  outfile << "ini1: " << logCondConfig["block1.mean"] << std::endl;
  outfile << "ini2: " << logCondConfig["block2.mean"] << std::endl;*/
  outfile << "logCond: " << -20.0 << " (out of bounds)" << std::endl;
}

        return -20.0; // Let's enforce a reject when rectangle out of domain
      }
      param_cnt += 5;
    }


    try {
      modelList->solveForward(parameterList,measurementList);
    } catch (Dune::Exception & e) {
{
  std::ofstream outfile;
  if(eval_cnt > 1)
    outfile.open(name_prefix_ + "_logdens.txt", std::ios_base::app);
  else
    outfile.open(name_prefix_ + "_logdens.txt", std::ios_base::out | std::ios_base::trunc);
  /*  outfile << "state: " << toString(state->state.at(0)) << std::endl;
    *  outfile << "ini0: " << logCondConfig["block0.mean"] << std::endl;
    *  outfile << "ini1: " << logCondConfig["block1.mean"] << std::endl;
    *  outfile << "ini2: " << logCondConfig["block2.mean"] << std::endl;*/
  outfile << "logCond: " << -20.0 << " " << e.what() << std::endl;
}
      return -20.0;
    }
    std::cout << "parameterList objective function: " << parameterList->objectiveFunction() << std::endl;
    std::cout << "measurementList objective function: " << measurementList->objectiveFunction() << std::endl;
    //double objFunc = parameterList->objectiveFunction() + measurementList->objectiveFunction(); // NOTE: Only posterior in block model!!
    double objFunc = measurementList->objectiveFunction();
    std::cout << "objective function: " << objFunc << std::endl;

{
  std::ofstream outfile;
if(eval_cnt > 1)
  outfile.open(name_prefix_ + "_logdens.txt", std::ios_base::app);
else
  outfile.open(name_prefix_ + "_logdens.txt", std::ios_base::out | std::ios_base::trunc);
/*outfile << "state: " << toString(state->state.at(0)) << std::endl;
outfile << "ini0: " << logCondConfig["block0.mean"] << std::endl;
outfile << "ini1: " << logCondConfig["block1.mean"] << std::endl;
outfile << "ini2: " << logCondConfig["block2.mean"] << std::endl;*/
outfile << "logCond: " << -objFunc / 100.0 << std::endl;
}

//exit(0);
    return -objFunc / 100.0;
    //return .1;
  }

  void writeVTKfromQOI(Eigen::VectorXd data, std::string filename) {
    V u(*gfs,0.0);
    using Dune::PDELab::Backend::native;
    for (int i = 0; i < u.N(); i++)
      native(u)[i] = data(i);

    Dune::VTKWriter<GV> vtkwriter(*gv);
    typedef Dune::PDELab::DiscreteGridFunction<GFS,V> DGF;
    DGF xdgf(*gfs,u);
    typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> ADAPT;
    auto adapt = std::make_shared<ADAPT>(xdgf,"perm_field");
    vtkwriter.addVertexData(adapt);
    vtkwriter.write(filename);

  }

  virtual std::shared_ptr<SamplingState> QOI() override {
    return std::make_shared<SamplingState>(qoi, 1.0);
  }

private:

  Eigen::VectorXd qoi;

  std::shared_ptr<SamplingState> lastState = nullptr;


  std::shared_ptr<muq::Modeling::ModPiece> target;

  std::string name_prefix_;

  Dune::MPIHelper& mpi_helper_;

  std::shared_ptr<parcer::Communicator> comm_;
};


class MyInterpolation : public MIInterpolation {
public:
  std::shared_ptr<SamplingState> Interpolate (std::shared_ptr<SamplingState> coarseProposal, std::shared_ptr<SamplingState> fineProposal) {
    return std::make_shared<SamplingState>(coarseProposal->state);
  }
};

class MyMIComponentFactory : public ParallelizableMIComponentFactory {
public:
  MyMIComponentFactory (pt::ptree pt, Dune::MPIHelper& mpi_helper, std::shared_ptr<parcer::Communicator> global_comm)
   : pt(pt), mpi_helper_(mpi_helper), global_comm(global_comm)
  { }

  void SetComm(std::shared_ptr<parcer::Communicator> const& comm) override {
    this->comm = comm;
  }

  virtual std::shared_ptr<MCMCProposal> Proposal (std::shared_ptr<MultiIndex> const& index, std::shared_ptr<AbstractSamplingProblem> const& samplingProblem) override {
    pt::ptree pt_prop;
    pt_prop.put("BlockIndex",0);

    Eigen::VectorXd mu(ParamDim(index));
    mu.setZero();
    //mu << 1.0, 2.0;
    Eigen::MatrixXd cov = Eigen::MatrixXd::Identity(ParamDim(index),ParamDim(index));
    cov *= .0001;
    for (int i = 4; i < ParamDim(index); i+=5) {
      cov(i,i)*= 1e2;
    }
    /*Eigen::MatrixXd cov(2,2);
    cov << 0.7, 0.6,
    0.6, 1.0;
    cov *= 20.0;*/

    auto prior = std::make_shared<Gaussian>(mu, cov);

    //return std::make_shared<CrankNicolsonProposal>(pt_prop, samplingProblem, prior);
    return std::make_shared<MHProposal>(pt_prop, samplingProblem, prior);
  }

  virtual std::shared_ptr<MultiIndex> FinestIndex() override {
    auto index = std::make_shared<MultiIndex>(1);
    index->SetValue(0, 0);
    //index->SetValue(0, 1);
    return index;
  }

  virtual std::shared_ptr<MCMCProposal> CoarseProposal (std::shared_ptr<MultiIndex> const& index,
                                                        std::shared_ptr<AbstractSamplingProblem> const& coarseProblem,
                                                        std::shared_ptr<SingleChainMCMC> const& coarseChain) override {
    pt::ptree ptProposal;
    ptProposal.put("BlockIndex",0);
    //int subsampling = 5;
    ptProposal.put("Subsampling", pt.get<int>("MLMCMC.Subsampling"));
    return std::make_shared<SubsamplingMIProposal>(ptProposal, coarseProblem, coarseChain);
  }

  virtual std::shared_ptr<AbstractSamplingProblem> SamplingProblem (std::shared_ptr<MultiIndex> const& index) override {
    return std::make_shared<MySamplingProblem>(mpi_helper_, index, comm, "lvl" + std::to_string(index->GetValue(0)) + "proc" + std::to_string(global_comm->GetRank()));
  }

  virtual std::shared_ptr<MIInterpolation> Interpolation (std::shared_ptr<MultiIndex> const& index) override {
    return std::make_shared<ConcatenatingInterpolation>(index);
  }

  virtual Eigen::VectorXd StartingPoint (std::shared_ptr<MultiIndex> const& index) override {
    Eigen::VectorXd mu(ParamDim(index));
    mu.setZero();

    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::uniform_real_distribution<> dx(0.1, 0.19);
    std::uniform_real_distribution<> dy(0.3, 0.4);
    std::uniform_real_distribution<> dw(0.05, 0.1);
    std::uniform_real_distribution<> dh(0.05, 0.1);
    std::uniform_real_distribution<> dp(-5.0, -1.0);
    int param_cnt = 0;
    for (int blockid = 0; blockid < 3; blockid++) {
      mu(param_cnt+0) = dx(gen);
      mu(param_cnt+1) = dy(gen);
      mu(param_cnt+2) = dw(gen);
      mu(param_cnt+3) = dh(gen);
      mu(param_cnt+4) = dp(gen);
      param_cnt += 5;
    }
    //for (int i = 0; i < ParamDim(index); i++)
    //  mu(i) = d(gen);

    //mu << 1.0, 2.0;
    return mu;
  }

private:
  pt::ptree pt;
  Dune::MPIHelper& mpi_helper_;
  std::shared_ptr<parcer::Communicator> comm;
  std::shared_ptr<parcer::Communicator> global_comm;
};

