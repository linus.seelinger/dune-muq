#include "MUQ/SamplingAlgorithms/ParallelizableMIComponentFactory.h"
#include<dune/modelling/forwardadjointmodel.hh>
#include <fstream>
#include "MUQ/SamplingAlgorithms/ConcatenatingInterpolation.h"


//int PARAM_DIM = 5*5*2;
//int PARAM_DIM = 15*15*2;
int ParamDim(std::shared_ptr<MultiIndex> index) {
  if (index->GetValue(0) == 0)
    return 15*15*2;
  if (index->GetValue(0) == 1)
    return 21*21*2;
  spdlog::error("unknown model requested in ParamDim");
  exit(-1);
}

class MySamplingProblem : public AbstractSamplingProblem {
public:
  MySamplingProblem(Dune::MPIHelper& mpi_helper, std::shared_ptr<MultiIndex> index, std::shared_ptr<parcer::Communicator> comm, std::string name_prefix)
   : AbstractSamplingProblem(Eigen::VectorXi::Constant(1,ParamDim(index)), Eigen::VectorXi::Constant(1,ParamDim(index))),
     mpi_helper_(mpi_helper),
     comm_(comm), name_prefix_(name_prefix) {

    Dune::ParameterTreeParser parser;
    parser.readINITree("inversion.ini",config);
    if (index->GetValue(0) == 0)
      config["grid.cells"] = "32 64";
    else if (index->GetValue(0) == 1)
      config["grid.cells"] = "64 128";
      //config["grid.cells"] = "64 128";
    else
      exit(-1);


    traits = std::make_shared<MyInversionTraits>(comm_->GetMPICommunicator(),config);
    //traits = std::make_shared<MyInversionTraits>(mpi_helper.getCommunicator(),config);
    //traits = std::make_shared<MyInversionTraits>(mpi_helper_,config);

    if (traits->rank() == 0)
    {
      std::cout << "------------------" << std::endl;
      std::cout << "filling model list" << std::endl;
      std::cout << "------------------" << std::endl;
    }


    modelList = std::make_shared<ModelList>(*traits);
    modelList->template add<FlowModel>("head");
    modelList->template add<TransportModel,FlowModel>("tracer",{"head"});
    if (traits->rank() == 0) modelList->report(std::cout);

    Dune::ParameterTree measConfig = config;
    measConfig["randomField.types"] = config["measurements.types"];
//    measConfig["randomField.active"] = config["measurements.active"];
    measConfig["grid.cells"]        = config["measurements.cells"];
    const int dim = 2;
    measurementList = std::make_shared<MeasurementList>(measConfig,measConfig.get<std::string>("measurements.referenceName",""),Dune::RandomField::DefaultLoadBalance<dim>(),comm_->GetMPICommunicator());

    if (traits->rank() == 0)
    {
      std::cout << "-------------------" << std::endl;
      std::cout << "reading input field" << std::endl;
      std::cout << "-------------------" << std::endl;
    }

    Dune::ParameterTree paramConfig = config;
    paramConfig["randomField.types"] = config["parameters.types"];
    paramConfig["randomField.active"] = config["parameters.active"];
    paramConfig["grid.cells"]        = config["parameters.cells"];
    //paramConfig["grid.cells"]        = config["grid.cells"]; // Use same dims as FE grid

    parameterList = std::make_shared<ParameterList>(paramConfig,paramConfig.get<std::string>("paramField.inputName",""),Dune::RandomField::DefaultLoadBalance<dim>(),comm_->GetMPICommunicator());
  }

  virtual ~MySamplingProblem() {
    modelList = nullptr; //Need this because modelList destructor assumes *lists to exist
    measurementList = nullptr;
    parameterList = nullptr;
    traits = nullptr;
  }

  int eval_cnt = 0;

  using DF = double;
  using RF = double;

  using MyGridTraits      = GridTraits<DF,RF,2>;
  using MyInversionTraits = InversionTraits<MyGridTraits>;

  using FlowModelType = ModelType::Groundwater;
  using FormulationType= Dune::Modelling::Formulation::Transient;

  using ModelList = Dune::Modelling::ForwardModelList<MyInversionTraits>;
  using FlowModel = Dune::Modelling::ForwardModel<MyInversionTraits,FlowModelType,FormulationType>;
  using TransportModel = Dune::Modelling::ForwardModel<MyInversionTraits,ModelType::Transport,FormulationType>;
  using ParameterList   = typename MyInversionTraits::ParameterList;
  using MeasurementList = typename MyInversionTraits::MeasurementList;

  Dune::ParameterTree config;
  std::shared_ptr<ParameterList> parameterList;
  std::shared_ptr<MeasurementList> measurementList;
  std::shared_ptr<ModelList> modelList;
  std::shared_ptr<MyInversionTraits> traits;

  virtual double LogDensity(unsigned int const t, std::shared_ptr<SamplingState> const& state, AbstractSamplingProblem::SampleType type) override {
    lastState = state;


    auto field = parameterList->get("logCond");

    typedef ParameterList::SubRandomField::Traits RndFieldTraits;
    field->generate(42, [&](fftw_complex* extendedField, const auto* matrix, std::shared_ptr<RndFieldTraits> traits) {

      std::vector<Eigen::VectorXd> const& parameters = state->state;

      const std::array<unsigned int,RndFieldTraits::dim>& extendedCells = matrix->getExtendedCells();
      const std::array<unsigned int,RndFieldTraits::dim>& localExtendedCells = matrix->getLocalExtendedCells();
      const std::array<unsigned int,RndFieldTraits::dim>& localExtendedOffset = matrix->getLocalExtendedOffset();

      for (unsigned int index = 0; index < matrix->getLocalExtendedDomainSize(); index++)
      {
        extendedField[index][0] = 0.0;
        extendedField[index][1] = 0.0;
      }

      int c = 0;
      for (int l = 0; parameters.at(0).rows() > c; l++) {
      //for (int l = 0; l <= PARAM_SIZE[index->GetValue(0)]; l++) {
        for (int y = -l; y <= l; y++) {
          for (int x = -l; x <= l; x++) {

            int rx = x, ry = y;

            if (rx < 0)
              rx += extendedCells[0];
            if (ry < 0)
              ry += extendedCells[1];

            rx -= localExtendedOffset[0];
            ry -= localExtendedOffset[1];

            if (rx >= 0 && ry >= 0 && rx < localExtendedCells[0] && ry < localExtendedCells[1]) {

              std::array<unsigned int,RndFieldTraits::dim> indices = {(unsigned int)rx, (unsigned int)ry};
              unsigned int index = traits->indicesToIndex(indices, matrix->getLocalExtendedCells());

              //index =
              //std::cout << "writing #" << c << "\t at" << "\t" << x << "\t" << y << "\t" << rx << "\t" << ry << "\tidx " << index << "\t val:" << parameters.at(0)(c) << std::endl;
              assert (parameters.at(0).rows() > c);
              assert (index < matrix->getLocalExtendedDomainSize());
              assert (index >= 0);

              double lambda = std::sqrt(std::abs(matrix->fftTransformedMatrix[index][0]) / matrix->getExtendedDomainSize());

              extendedField[index][0] = lambda * parameters.at(0)(c);
              extendedField[index][1] = lambda * parameters.at(0)(c+1); // FIXME: Separate parameter!!
            }


            c+=2;

            if (std::abs(y) != l)
              x += 2*l-1;
          }
        }

      }

    }, true);

    //parameterList->zero();

    //parameterList->writeToFile("param_" + std::to_string(eval_cnt));
    parameterList->writeToVTK(name_prefix_ + "_param_" + std::to_string(eval_cnt), "param", traits->grid().leafGridView());


    modelList->solveForward(parameterList,measurementList);
    std::cout << "parameterList objective function: " << parameterList->objectiveFunction() << std::endl;
    std::cout << "measurementList objective function: " << measurementList->objectiveFunction() << std::endl;
    double objFunc = parameterList->objectiveFunction() + measurementList->objectiveFunction();
    std::cout << "objective function: " << objFunc << std::endl;

{
  std::ofstream outfile;
if(eval_cnt > 0)
  outfile.open(name_prefix_ + "_logdens.txt", std::ios_base::app);
else
  outfile.open(name_prefix_ + "_logdens.txt", std::ios_base::out | std::ios_base::trunc);
  outfile << -objFunc / 100.0 << std::endl; 
}

    eval_cnt++;
//exit(0);
    return -objFunc / 100.0;
    //return .1;
  }

  virtual std::shared_ptr<SamplingState> QOI() override {
    assert (lastState != nullptr);
    return std::make_shared<SamplingState>(lastState->state, 1.0);
  }

private:
  std::shared_ptr<SamplingState> lastState = nullptr;


  std::shared_ptr<muq::Modeling::ModPiece> target;

  std::string name_prefix_;

  Dune::MPIHelper& mpi_helper_;

  std::shared_ptr<parcer::Communicator> comm_;
};


class MyInterpolation : public MIInterpolation {
public:
  std::shared_ptr<SamplingState> Interpolate (std::shared_ptr<SamplingState> coarseProposal, std::shared_ptr<SamplingState> fineProposal) {
    return std::make_shared<SamplingState>(coarseProposal->state);
  }
};

class MyMIComponentFactory : public ParallelizableMIComponentFactory {
public:
  MyMIComponentFactory (pt::ptree pt, Dune::MPIHelper& mpi_helper, std::shared_ptr<parcer::Communicator> global_comm)
   : pt(pt), mpi_helper_(mpi_helper), global_comm(global_comm)
  { }

  void SetComm(std::shared_ptr<parcer::Communicator> const& comm) override {
    this->comm = comm;
  }

  virtual std::shared_ptr<MCMCProposal> Proposal (std::shared_ptr<MultiIndex> const& index, std::shared_ptr<AbstractSamplingProblem> const& samplingProblem) override {
    pt::ptree pt_prop;
    pt_prop.put("BlockIndex",0);

    Eigen::VectorXd mu(ParamDim(index));
    mu.setZero();
    //mu << 1.0, 2.0;
    Eigen::MatrixXd cov = Eigen::MatrixXd::Identity(ParamDim(index),ParamDim(index));
    cov *= 10;
    /*Eigen::MatrixXd cov(2,2);
    cov << 0.7, 0.6,
    0.6, 1.0;
    cov *= 20.0;*/

    auto prior = std::make_shared<Gaussian>(mu, cov);

    return std::make_shared<CrankNicolsonProposal>(pt_prop, samplingProblem, prior);
  }

  virtual std::shared_ptr<MultiIndex> FinestIndex() override {
    auto index = std::make_shared<MultiIndex>(1);
    //index->SetValue(0, 1);
    index->SetValue(0, 1);
    return index;
  }

  virtual std::shared_ptr<MCMCProposal> CoarseProposal (std::shared_ptr<MultiIndex> const& index,
                                                        std::shared_ptr<AbstractSamplingProblem> const& coarseProblem,
                                                        std::shared_ptr<SingleChainMCMC> const& coarseChain) override {
    pt::ptree ptProposal;
    ptProposal.put("BlockIndex",0);
    //int subsampling = 5;
    ptProposal.put("Subsampling", pt.get<int>("MLMCMC.Subsampling"));
    return std::make_shared<SubsamplingMIProposal>(ptProposal, coarseProblem, coarseChain);
  }

  virtual std::shared_ptr<AbstractSamplingProblem> SamplingProblem (std::shared_ptr<MultiIndex> const& index) override {
    return std::make_shared<MySamplingProblem>(mpi_helper_, index, comm, "lvl" + std::to_string(index->GetValue(0)) + "proc" + std::to_string(global_comm->GetRank()));
  }

  virtual std::shared_ptr<MIInterpolation> Interpolation (std::shared_ptr<MultiIndex> const& index) override {
    return std::make_shared<ConcatenatingInterpolation>(index);
  }

  virtual Eigen::VectorXd StartingPoint (std::shared_ptr<MultiIndex> const& index) override {
    Eigen::VectorXd mu(ParamDim(index));
    mu.setZero();

    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::normal_distribution<> d(0, 4);
    for (int i = 0; i < ParamDim(index); i++)
      mu(i) = d(gen);

    //mu << 1.0, 2.0;
    return mu;
  }

private:
  pt::ptree pt;
  Dune::MPIHelper& mpi_helper_;
  std::shared_ptr<parcer::Communicator> comm;
  std::shared_ptr<parcer::Communicator> global_comm;
};

