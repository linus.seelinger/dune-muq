#ifndef GROUNDWATERTRAITS_HH
#define GROUNDWATERTRAITS_HH

#include<dune/modelling/equation.hh>
#include<dune/modelling/solvers.hh>

#include<src/groundwater/localoperatorDG.hh>
#include<src/groundwater/parameters.hh>

namespace Dune {
  namespace Modelling {

    template<typename InversionTraits, typename FormulationType, typename DirectionType>
      class Equation<InversionTraits,ModelType::Groundwater,FormulationType,DirectionType>
      : public DifferentialEquation<InversionTraits,ModelType::Groundwater,FormulationType,DirectionType>
      {
        using DifferentialEquation<InversionTraits,ModelType::Groundwater,FormulationType,DirectionType>::DifferentialEquation;
      };

    /**
     * @brief Traits class with definitions for groundwater equation
     */
    template<typename InversionTraits, typename DirectionType>
      class EquationTraits<InversionTraits, ModelType::Groundwater, DirectionType>
      {
        public:

          using GridTraits = typename InversionTraits::GridTraits;

          using GridView    = typename GridTraits::GridView;
          using DomainField = typename GridTraits::DomainField;
          using RangeField  = typename GridTraits::RangeField;

          enum {dim = GridTraits::dim};
          enum {order = 1};

          //using DiscretizationType = Discretization::CellCenteredFiniteVolume;
          using DiscretizationType = Discretization::DiscontinuousGalerkin;

          template<typename... T>
            using StationarySolver = StationaryLinearSolver<T...>;
          template<typename... T>
            using TransientSolver = ImplicitLinearSolver<T...>;

          template<typename... T>
            using FluxReconstruction = RT0Reconstruction<T...>;
          template<typename... T>
            using StorageContainer = FullContainer<T...>;
          template<typename... T>
            using TemporalInterpolation = NextTimestep<T...>;

          /*
          typedef Dune::PDELab::P0LocalFiniteElementMap<DomainField,RangeField,dim> FEM;
          typedef Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed,1> VBE;
          */
          typedef Dune::PDELab::QkDGLocalFiniteElementMap<DomainField,RangeField,order,dim> FEM;
          typedef Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed,Dune::QkStuff::QkSize<order,dim>::value> VBE;
          typedef Dune::PDELab::P0ParallelConstraints CON;
          //typedef Dune::PDELab::Alexander2Parameter<RangeField> OneStepScheme;
          typedef Dune::PDELab::ImplicitEulerParameter<RangeField> OneStepScheme;

          typedef Dune::PDELab::GridFunctionSpace<GridView,FEM,CON,VBE> GridFunctionSpace;
          typedef typename Dune::PDELab::Backend::Vector<GridFunctionSpace,RangeField> GridVector;

        private:

          const FEM fem;
          GridFunctionSpace space;

        public:

          EquationTraits(const InversionTraits& invTraits)
              : fem(), space(invTraits.grid().levelGridView(0),fem)
          {}

          const GridFunctionSpace& gfs() const
          {
            return space;
          }
      };

  }
}

#endif //GROUNDWATERTRAITS_HH
