#ifndef DUNE_INVERSION_GROUNDWATER_OPERATOR_DG_HH
#define DUNE_INVERSION_GROUNDWATER_OPERATOR_DG_HH

#include<dune/pdelab/common/quadraturerules.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/finiteelement/localbasiscache.hh>

#include<dune/modelling/declarations.hh>
#include<dune/modelling/boundary.hh>

#include<dune/inversion/parameters.hh>
#include<src/groundwater/parameters.hh>

namespace Dune {
  namespace Modelling {

    struct GroundwaterDGMethod
    {
      enum Type { NIPG, SIPG, IIPG };
    };

    struct GroundwaterDGWeights
    {
      enum Type { weightsOn, weightsOff };
    };

    /**
     * @brief DG local operator for the spatial part of the groundwater flow equation
     */
    template<typename InversionTraits, typename DirectionType>
      class SpatialOperator<InversionTraits, ModelType::Groundwater, Discretization::DiscontinuousGalerkin, DirectionType>
      : public Dune::PDELab::NumericalJacobianVolume<SpatialOperator<InversionTraits, ModelType::Groundwater, Dune::Modelling::Discretization::DiscontinuousGalerkin, DirectionType> >,
      public Dune::PDELab::NumericalJacobianSkeleton<SpatialOperator<InversionTraits, ModelType::Groundwater, Dune::Modelling::Discretization::DiscontinuousGalerkin, DirectionType> >,
      public Dune::PDELab::NumericalJacobianBoundary<SpatialOperator<InversionTraits, ModelType::Groundwater, Dune::Modelling::Discretization::DiscontinuousGalerkin, DirectionType> >,
      public Dune::PDELab::FullSkeletonPattern,
      public Dune::PDELab::FullVolumePattern,
      public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<typename InversionTraits::GridTraits::RangeField>
      {
        using GridTraits = typename InversionTraits::GridTraits;

        enum { dim = GridTraits::dim };

        using RF       = typename GridTraits::RangeField;
        using Scalar   = typename GridTraits::Scalar;
        using Vector   = typename GridTraits::Vector;
        using Tensor   = typename GridTraits::Tensor;
        using DF       = typename GridTraits::DomainField;
        using Domain   = typename GridTraits::Domain;
        using IDomain  = typename GridTraits::IDomain;
        using Element  = typename GridTraits::Element;

        using FEM            = typename EquationTraits<InversionTraits,ModelType::Groundwater,DirectionType>::FEM;
        using LocalBasisType = typename FEM::Traits::FiniteElementType::Traits::LocalBasisType;
        using Cache          = Dune::PDELab::LocalBasisCache<LocalBasisType>;

        public:

        // pattern assembly flags
        enum { doPatternVolume = true };
        enum { doPatternSkeleton = true };

        // residual assembly flags
        enum { doAlphaVolume  = true };
        enum { doAlphaSkeleton  = true };
        enum { doAlphaBoundary  = true };
        enum { doLambdaVolume   = true };

        const ModelParameters<InversionTraits,ModelType::Groundwater>& param;
        const Boundary       <InversionTraits,ModelType::Groundwater,DirectionType> boundary;
        const SourceTerm     <InversionTraits,ModelType::Groundwater,DirectionType> sourceTerm;

        RF time;
        GroundwaterDGMethod::Type method;
        GroundwaterDGWeights::Type weights;
        RF penalty_factor;
        int intorderadd;
        int quadrature_factor;
        RF theta;

        // In theory it is possible that one and the same local operator is
        // called first with a finite element of one type and later with a
        // finite element of another type.  Since finite elements of different
        // type will usually produce different results for the same local
        // coordinate they cannot share a cache.  Here we use a vector of caches
        // to allow for different orders of the shape functions, which should be
        // enough to support p-adaptivity.  (Another likely candidate would be
        // differing geometry types, i.e. hybrid meshes.)

        std::vector<Cache> cache;

        /**
         * @brief Constructor
         */
        SpatialOperator (const InversionTraits& traits, const ModelParameters<InversionTraits,ModelType::Groundwater>& param_,
            //GroundwaterDGMethod::Type method_ = GroundwaterDGMethod::NIPG,
            GroundwaterDGMethod::Type method_ = GroundwaterDGMethod::SIPG,
            //GroundwaterDGMethod::Type method_ = GroundwaterDGMethod::IIPG,
            //GroundwaterDGWeights::Type weights_ = GroundwaterDGWeights::weightsOff,
            GroundwaterDGWeights::Type weights_ = GroundwaterDGWeights::weightsOn)
          : param(param_), boundary(traits,param.name()), sourceTerm(traits.config(),param), method(method_), weights(weights_),
          penalty_factor   (traits.config().template get<RF>("dg.penalty_factor")),
          intorderadd      (traits.config().template get<RF>("dg.intorderadd")),
          quadrature_factor(traits.config().template get<RF>("dg.quadrature_factor")),
          cache(20)
        {
          theta = 1.;
          if (method == GroundwaterDGMethod::SIPG) theta = -1.;
          if (method == GroundwaterDGMethod::IIPG) theta = 0.;
        }

        /**
         * @brief Volume integral depending on test and ansatz functions
         */
        template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
          void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
          {
            const int order = lfsu.finiteElement().localBasis().order();
            const int intorder = intorderadd + quadrature_factor * order;

            Dune::DiagonalMatrix<DF,dim> jac;
            std::vector<Vector> gradphi(lfsu.size());
            Vector gradu(0.);

            // loop over quadrature points
            for (const auto& point : quadratureRule(eg.geometry(),intorder))
            {
              // evaluate gradient of basis functions (we assume Galerkin method lfsu = lfsv)
              using JacobianType = typename LFSU::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType;
              const std::vector<JacobianType>& js = cache[order].evaluateJacobian(point.position(),lfsu.finiteElement().localBasis());

              // transform gradients to real element
              jac = eg.geometry().jacobianInverseTransposed(point.position());
              for (unsigned int i = 0; i < lfsu.size(); i++)
                jac.mv(js[i][0],gradphi[i]);

              // compute gradient of u
              gradu = 0.;
              for (unsigned int i = 0; i < lfsu.size(); i++)
                gradu.axpy(x(lfsu,i),gradphi[i]);

              // compute conductivity
              const RF cond = param.K(eg.entity(),point.position());

              // integration factor
              const RF factor = point.weight() * eg.geometry().integrationElement(point.position());

              // update residual
              for (unsigned int i = 0; i < lfsv.size(); i++)
                r.accumulate(lfsv,i, cond * (gradu*gradphi[i]) * factor);

              if (DirectionType::isAdjoint())
              {
                const Vector& adjGrad = param.adjTracerFlux(eg.entity(),point.position(),time);

                for (unsigned int i = 0; i < lfsv.size(); i++)
                  r.accumulate(lfsv,i, cond * (adjGrad*gradphi[i]) * factor);
              }
            }
          }

        /**
         * @brief Skeleton integral depending on test and ansatz functions
         */
        // each face is only visited ONCE!
        template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
          void alpha_skeleton (const IG& ig,
              const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
              const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
              R& r_s, R& r_n) const
          {
            // get polynomial degree
            const int order_s  = lfsu_s.finiteElement().localBasis().order();
            const int order_n  = lfsu_n.finiteElement().localBasis().order();
            const int degree   = std::max( order_s, order_n );
            const int intorder = intorderadd + quadrature_factor * degree;

            // geometric factor of penalty
            const RF h_F = std::min(ig.inside().geometry().volume(),ig.outside().geometry().volume())/ig.geometry().volume();

            Dune::DiagonalMatrix<DF,dim> jac;
            std::vector<Vector> tgradphi_s(lfsu_s.size());
            std::vector<Vector> tgradphi_n(lfsu_n.size());
            Vector gradu_s(0.);
            Vector gradu_n(0.);

            // loop over quadrature points and integrate normal flux
            for (const auto& point : quadratureRule(ig.geometry(),intorder))
            {
              // position of quadrature point in local coordinates of elements
              const Domain local_s = ig.geometryInInside().global(point.position());
              const Domain local_n = ig.geometryInOutside().global(point.position());

              // evaluate basis functions
              const std::vector<Scalar>& phi_s = cache[order_s].evaluateFunction(local_s,lfsu_s.finiteElement().localBasis());
              const std::vector<Scalar>& phi_n = cache[order_n].evaluateFunction(local_n,lfsu_n.finiteElement().localBasis());

              // evaluate u
              RF u_s = 0., u_n = 0.;
              for (unsigned int i = 0; i < lfsu_s.size(); i++)
                u_s += x_s(lfsu_s,i) * phi_s[i];
              for (unsigned int i = 0; i < lfsu_n.size(); i++)
                u_n += x_n(lfsu_n,i) * phi_n[i];

              // evaluate gradient of basis functions (we assume Galerkin method lfsu = lfsv)
              using JacobianType = typename LFSU::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType;
              const std::vector<JacobianType>& gradphi_s = cache[order_s].evaluateJacobian(local_s,lfsu_s.finiteElement().localBasis());
              const std::vector<JacobianType>& gradphi_n = cache[order_n].evaluateJacobian(local_n,lfsu_n.finiteElement().localBasis());

              // transform gradients to real element
              jac = ig.inside().geometry().jacobianInverseTransposed(local_s);
              for (unsigned int i = 0; i < lfsu_s.size(); i++)
                jac.mv(gradphi_s[i][0],tgradphi_s[i]);
              jac = ig.outside().geometry().jacobianInverseTransposed(local_n);
              for (unsigned int i = 0; i < lfsu_n.size(); i++)
                jac.mv(gradphi_n[i][0],tgradphi_n[i]);

              // compute gradient of u
              gradu_s = 0.;
              for (unsigned int i = 0; i < lfsu_s.size(); i++)
                gradu_s.axpy(x_s(lfsu_s,i),tgradphi_s[i]);
              gradu_n = 0.;
              for (unsigned int i = 0; i < lfsu_n.size(); i++)
                gradu_n.axpy(x_n(lfsu_n,i),tgradphi_n[i]);

              // face normal vector
              const Domain normal = ig.unitOuterNormal(point.position());

              // compute jump in solution
              const RF jump = u_s - u_n;

              // conductivity
              const RF cond_s = param.K(ig.inside(),local_s);
              const RF cond_n = param.K(ig.outside(),local_n);

              // compute weights
              RF omega_s = 0.5, omega_n = 0.5;
              if(weights == GroundwaterDGWeights::weightsOn)
              {
                omega_s = cond_n / (cond_s + cond_n);
                omega_n = cond_s / (cond_s + cond_n);
              }

              // integration factor
              const RF factor = point.weight() * ig.geometry().integrationElement(point.position());

              const RF penalty = penalty_factor / h_F * degree * (degree + dim - 1);

              // compute numerical flux
              const RF numFlux_s = cond_s * ( - (gradu_s * normal) + penalty * jump);
              const RF numFlux_n = cond_n * ( - (gradu_n * normal) + penalty * jump);
              const RF numFlux   = omega_s * numFlux_s + omega_n * numFlux_n;

              // update residual (flux term)
              // diffusion term
              // consistency term
              // + penalty term
              for (unsigned int i = 0; i < lfsv_s.size(); i++)
                r_s.accumulate(lfsv_s,i,   numFlux * phi_s[i] * factor);
              for (unsigned int i = 0; i < lfsv_n.size(); i++)
                r_n.accumulate(lfsv_n,i, - numFlux * phi_n[i] * factor);

              // update residual (symmetry term)
              // (non-)symmetric IP term
              // symmetry term
              for (unsigned int i = 0; i < lfsv_s.size(); i++)
                r_s.accumulate(lfsv_s,i, theta * omega_s * (tgradphi_s[i] * normal) * cond_s * jump * factor);
              for (unsigned int i = 0; i < lfsv_n.size(); i++)
                r_n.accumulate(lfsv_n,i, theta * omega_n * (tgradphi_n[i] * normal) * cond_n * jump * factor);

              if (DirectionType::isAdjoint())
              {
                const Vector adjGrad_s = param.adjTracerFlux(ig.inside(), local_s,time);
                const Vector adjGrad_n = param.adjTracerFlux(ig.outside(),local_n,time);

                const RF adjTracerFlux_s = cond_s * ( - (adjGrad_s * normal));
                const RF adjTracerFlux_n = cond_n * ( - (adjGrad_n * normal));
                const RF adjTracerFlux = omega_s * adjTracerFlux_s + omega_n * adjTracerFlux_n;

                for (unsigned int i = 0; i < lfsv_s.size(); i++)
                  r_s.accumulate(lfsv_s,i,   adjTracerFlux * phi_s[i] * factor);
                for (unsigned int i = 0; i < lfsv_n.size(); i++)
                  r_n.accumulate(lfsv_n,i, - adjTracerFlux * phi_n[i] * factor);
              }
            }
          }

        /**
         * @brief Boundary integral depending on test and ansatz functions
         */
        template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
          void alpha_boundary (const IG& ig,
              const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
              R& r_s) const
          {
            // geometry information
            const RF faceVolume   = ig         .geometry().volume();
            const RF insideVolume = ig.inside().geometry().volume();
            // get polynomial degree
            const int order_s  = lfsu_s.finiteElement().localBasis().order();
            const int degree   = order_s;
            const int intorder = intorderadd + quadrature_factor * degree;

            // penalty factor
            const RF h_F = insideVolume/faceVolume;
            const RF penalty = penalty_factor / h_F * degree * (degree + dim - 1);

            Dune::DiagonalMatrix<DF,dim> jac;
            std::vector<Vector> tgradphi_s(lfsu_s.size());
            Vector gradu_s(0.);

            // loop over quadrature points and integrate normal flux
            for (const auto& point : quadratureRule(ig.geometry(),intorder))
            {
              // position of quadrature point in local coordinates of elements
              const Domain local_s = ig.geometryInInside().global(point.position());

              // evaluate boundary condition
              BoundaryCondition::Type bc = boundary.bc(ig.intersection(),point.position(),time);

              // evaluate basis functions
              const std::vector<Scalar>& phi_s = cache[order_s].evaluateFunction(local_s,lfsu_s.finiteElement().localBasis());

              // integration factor
              const RF factor = point.weight() * ig.geometry().integrationElement(point.position());

              if (BoundaryCondition::isNeumann(bc))
              {
                // evaluate flux boundary condition
                const RF normal_flux = boundary.j(ig.intersection(),point.position(),time) * factor;

                // update residual (flux term)
                for (unsigned int i = 0; i < lfsv_s.size(); i++)
                  r_s.accumulate(lfsv_s,i, normal_flux * phi_s[i]);

                continue;
              }

              // evaluate u
              RF u_s = 0.;
              for (unsigned int i = 0; i < lfsu_s.size(); i++)
                u_s += x_s(lfsu_s,i) * phi_s[i];

              // evaluate gradient of basis functions (we assume Galerkin method lfsu = lfsv)
              using JacobianType = typename LFSV::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType;
              const std::vector<JacobianType>& gradphi_s = cache[order_s].evaluateJacobian(local_s,lfsu_s.finiteElement().localBasis());

              // transform gradients to real element
              jac = ig.inside().geometry().jacobianInverseTransposed(local_s);
              for (unsigned int i = 0; i < lfsu_s.size(); i++)
                jac.mv(gradphi_s[i][0],tgradphi_s[i]);

              // compute gradient of u
              gradu_s = 0.;
              for (unsigned int i = 0; i < lfsu_s.size(); i++)
                gradu_s.axpy(x_s(lfsu_s,i),tgradphi_s[i]);

              // face normal vector
              const Domain normal = ig.unitOuterNormal(point.position());

              // jump relative to Dirichlet value
              const RF g = boundary.g(ig.intersection(),point.position(),time);
              const RF jump = u_s - g;

              // conductivity
              const RF cond_s = param.K(ig.inside(),local_s);

              // compute numerical flux
              const RF numFlux = cond_s * ( - (gradu_s * normal) + penalty * jump);

              // update residual (flux term)
              // diffusion term
              // consistency term
              // + penalty term
              for (unsigned int i = 0; i < lfsv_s.size(); i++)
                r_s.accumulate(lfsv_s,i, numFlux * phi_s[i] * factor);

              // update residual (symmetry term)
              // (non-)symmetric IP term
              // symmetry term
              for (unsigned int i = 0; i < lfsv_s.size(); i++)
                r_s.accumulate(lfsv_s,i, theta * (tgradphi_s[i] * normal) * cond_s * jump * factor);
            }
          }

        /**
         * @brief Volume integral depending only on test functions
         */
        template<typename EG, typename LFSV, typename R>
          void lambda_volume (const EG& eg, const LFSV& lfsv, R& r) const
          {
            const int order = lfsv.finiteElement().localBasis().order();
            const int intorder = intorderadd + quadrature_factor * order;

            // loop over quadrature points
            for (const auto& point : quadratureRule(eg.geometry(),intorder))
            {

              // evaluate values of shape functions (we assume Galerkin method lfsu = lfsv)
              std::vector<Scalar> phi(lfsv.size());
              lfsv.finiteElement().localBasis().evaluateFunction(point.position(),phi);

              // scaled weight factor
              const RF factor = point.weight() * eg.geometry().integrationElement(point.position());

              //const RF source = sourceTerm.q(eg.entity(),point.position(),time) / eg.geometry().volume();
              const RF source = sourceTerm.q(eg.entity(),point.position(),time);

              //if (std::abs(source) > 1e-10)
              //  std::cout << "source " << eg.geometry().center()[0] << " " << eg.geometry().center()[1] << " " << source << std::endl;

              // update residual
              for (unsigned int i = 0; i < lfsv.size(); i++)
                r.accumulate(lfsv,i, - source * phi[i] * factor);

              //std::cout << "lambda " << - source << std::endl;

              if (DirectionType::isAdjoint())
              {
                const RF adjTracerSource = param.adjTracerSource(eg.entity(),point.position(),time);

                for (unsigned int i = 0; i < lfsv.size(); i++)
                  r.accumulate(lfsv,i, - adjTracerSource * phi[i] * factor);
              }
            }
          }

        //! set time
        void setTime (RF t)
        {
          time = t;
        }

      template<typename Intersection, typename ScalarDGF, typename LocalGradDGF>
        RF normalFluxDeriv(const Intersection& intersection, const IDomain& x, const ScalarDGF& scalar, const LocalGradDGF& localGradient, RF time) const
        {
          DUNE_THROW(Dune::NotImplemented,
              "derivative of normal flux not implemented for groundwater flow equation");
        }

      template<typename Intersection, typename ScalarDGF, typename LocalGradDGF>
        RF normalFlux(const Intersection& intersection, const IDomain& x, const ScalarDGF& scalar, const LocalGradDGF& localGradient, RF time) const
        {
          const Domain& xInside = intersection.geometryInInside().global(x);
          Scalar innerValue;
          (*scalar).evaluate(intersection.inside(),xInside,innerValue);
          Vector innerGrad;
          (*localGradient).evaluate(intersection.inside(),xInside,innerGrad);

          if (intersection.neighbor())
          {
            const Domain& xOutside = intersection.geometryInOutside().global(x);
            Scalar outerValue;
            (*scalar).evaluate(intersection.outside(),xOutside,outerValue);
            Vector outerGrad;
            (*localGradient).evaluate(intersection.outside(),xOutside,outerGrad);

            return skeletonNormalFlux(intersection,x,innerValue[0],outerValue[0],innerGrad,outerGrad,time);
          }
          else if (intersection.boundary())
          {
            // evaluate boundary condition type
            Dune::Modelling::BoundaryCondition::Type bc = boundary.bc(intersection,x,time);

            if (Dune::Modelling::BoundaryCondition::isDirichlet(bc))
              return dirichletNormalFlux(intersection,x,innerValue[0],innerGrad,time);
            else if (Dune::Modelling::BoundaryCondition::isNeumann(bc))
              return boundary.j(intersection,x,time);
            else
              DUNE_THROW(Dune::Exception,"unknown boundary condition type");
          }
          else
            return 0.;
        }

      template<typename Intersection, typename ScalarDGF, typename LocalGradDGF>
        RF normalDerivative(const Intersection& intersection, const IDomain& x, const ScalarDGF& scalar, const LocalGradDGF& localGradient, RF time) const
        {
          const Domain& xInside  = intersection.geometryInInside().global(x);
          Scalar innerValue;
          (*scalar).evaluate(intersection.inside(),xInside,innerValue);
          Vector innerGrad;
          (*localGradient).evaluate(intersection.inside(),xInside,innerGrad);

          if (intersection.neighbor())
          {
            const Domain& xOutside = intersection.geometryInOutside().global(x);
            Scalar outerValue;
            (*scalar).evaluate(intersection.outside(),xOutside,outerValue);
            Vector outerGrad;
            (*localGradient).evaluate(intersection.outside(),xOutside,outerGrad);

            return skeletonNormalDerivative(intersection,x,innerValue[0],outerValue[0],innerGrad,outerGrad);
          }
          else if (intersection.boundary())
          {
            // evaluate boundary condition type
            Dune::Modelling::BoundaryCondition::Type bc = boundary.bc(intersection,x,time);

            if (Dune::Modelling::BoundaryCondition::isDirichlet(bc))
              return dirichletNormalDerivative(intersection,x,innerValue[0],innerGrad,time);
            else if (Dune::Modelling::BoundaryCondition::isNeumann(bc))
            {
              const Domain& xInside = intersection.geometryInInside().global(x);
              const RF boundaryFlux = boundary.j(intersection,x,time);
              const RF K_inside = param.K(intersection.inside(),xInside);
              Scalar innerValue;
              (*scalar).evaluate(intersection.inside(), xInside,innerValue);
              // revert flux computation to obtain gradient on boundary
              return - boundaryFlux / K_inside;
            }
            else
              DUNE_THROW(Dune::Exception,"unknown boundary condition type");
          }
          else
            return 0.;
        }

        private:

      /**
       * @brief Flux in normal direction across interface
       */
      template<typename Intersection>
        RF skeletonNormalFlux(const Intersection& intersection, const IDomain& x, RF innerValue, RF outerValue, const Vector& innerGrad, const Vector& outerGrad, RF time) const
        {
          // geometry information
          const Domain& xInside  = intersection.geometryInInside() .global(x);
          const Domain& xOutside = intersection.geometryInOutside().global(x);

          const RF faceVolume    = intersection          .geometry().volume();
          const RF insideVolume  = intersection.inside() .geometry().volume();
          const RF outsideVolume = intersection.outside().geometry().volume();

          const Domain& normal = intersection.unitOuterNormal(x);

          // conductivity
          const RF K_inside  = param.K(intersection.inside(), xInside);
          const RF K_outside = param.K(intersection.outside(),xOutside);
          const RF K = havg(K_inside,K_outside);
          const RF omega_inside  = K_outside/(K_inside+K_outside+1e-20);
          const RF omega_outside = K_inside /(K_inside+K_outside+1e-20);

          // get polynomial degree
          //const int order_s  = lfsu_s.finiteElement().localBasis().order();
          //const int order_n  = lfsu_n.finiteElement().localBasis().order();
          //const int degree   = std::max( order_s, order_n );
          const int degree = 1;

          // penalty factor
          const RF h_F = std::min(insideVolume,outsideVolume)/faceVolume;
          RF penalty = (penalty_factor/h_F) * degree*(degree+dim-1);

          // compute jump in solution
          const RF jump = innerValue - outerValue;

          Vector adjGrad_inside(0.);
          Vector adjGrad_outside(0.);
          if (DirectionType::isAdjoint())
          {
            adjGrad_inside  = param.adjTracerFlux(intersection.inside(), xInside, time);
            adjGrad_outside = param.adjTracerFlux(intersection.outside(),xOutside,time);
          }

          return - (omega_inside * K_inside * (innerGrad * normal + adjGrad_inside * normal)
              + omega_outside * K_outside * (outerGrad * normal + adjGrad_outside * normal)
              - penalty * K * jump);
        }

      /**
       * @brief Derivative of solution in normal direction across interface
       */
      template<typename Intersection>
        RF skeletonNormalDerivative(const Intersection& intersection, const IDomain& x, RF innerValue, RF outerValue, const Vector& innerGrad, const Vector& outerGrad) const
        {
          // geometry information
          const RF faceVolume    = intersection          .geometry().volume();
          const RF insideVolume  = intersection.inside() .geometry().volume();
          const RF outsideVolume = intersection.outside().geometry().volume();

          const Domain& normal = intersection.unitOuterNormal(x);

          // get polynomial degree
          //const int order_s  = lfsu_s.finiteElement().localBasis().order();
          //const int order_n  = lfsu_n.finiteElement().localBasis().order();
          //const int degree   = std::max( order_s, order_n );
          const int degree = 1;

          // penalty factor
          const RF h_F = std::min(insideVolume,outsideVolume)/faceVolume;
          RF penalty = (penalty_factor/h_F) * degree*(degree+dim-1);
          //RF penalty = 1.;

          // compute jump in solution
          const RF jump = innerValue - outerValue;

          return 0.5 * (innerGrad * normal + outerGrad * normal) - penalty * jump;
        }

      /**
       * @brief Flux in normal direction on Dirichlet boundary
       */
      template<typename Intersection>
        RF dirichletNormalFlux(const Intersection& intersection, const IDomain& x, RF innerValue, const Vector& innerGrad, RF time) const
        {
          // geometry information
          const Domain& xInside = intersection.geometryInInside().global(x);

          const RF faceVolume    = intersection          .geometry().volume();
          const RF insideVolume  = intersection.inside() .geometry().volume();
          const RF outsideVolume = intersection.outside().geometry().volume();

          const Domain& normal = intersection.unitOuterNormal(x);

          const RF boundaryValue = boundary.g(intersection,x,time);

          // conductivity
          const RF K_inside = param.K(intersection.inside(),xInside);

          // get polynomial degree
          //const int order_s  = lfsu_s.finiteElement().localBasis().order();
          //const int order_n  = lfsu_n.finiteElement().localBasis().order();
          //const int degree   = std::max( order_s, order_n );
          const int degree = 1;

          // penalty factor
          const RF h_F = std::min(insideVolume,outsideVolume)/faceVolume;
          RF penalty = (penalty_factor/h_F) * degree*(degree+dim-1);

          // compute jump in solution
          const RF jump = innerValue - boundaryValue;

          return - K_inside * (innerGrad * normal - penalty * jump);
        }

      /**
       * @brief Derivative of solution in normal direction on Dirichlet boundary
       */
      template<typename Intersection>
        RF dirichletNormalDerivative(const Intersection& intersection, const IDomain& x, RF innerValue, const Vector& innerGrad, RF time) const
        {
          // geometry information
          const RF faceVolume    = intersection          .geometry().volume();
          const RF insideVolume  = intersection.inside() .geometry().volume();
          const RF outsideVolume = intersection.outside().geometry().volume();

          const Domain& normal = intersection.unitOuterNormal(x);

          // get polynomial degree
          //const int order_s  = lfsu_s.finiteElement().localBasis().order();
          //const int order_n  = lfsu_n.finiteElement().localBasis().order();
          //const int degree   = std::max( order_s, order_n );
          const int degree = 1;

          // penalty factor
          const RF h_F = std::min(insideVolume,outsideVolume)/faceVolume;
          RF penalty = (penalty_factor/h_F) * degree*(degree+dim-1);
          //RF penalty = 1.;

          // compute jump in solution
          const RF jump = innerValue - boundary.g(intersection,x,time);

          return innerGrad * normal - penalty * jump;
        }

      template<typename T>
        T havg (T a, T b) const
        {
          T eps = 1e-30;
          return 2./(1./(a+eps) + 1./(b+eps));
        }

      };

    /**
     * @brief DG local operator for the temporal derivative of the groundwater flow equation
     */
    template<typename InversionTraits, typename DirectionType>
      class TemporalOperator<InversionTraits, ModelType::Groundwater, Discretization::DiscontinuousGalerkin, DirectionType>
      : public Dune::PDELab::FullVolumePattern,
      public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<typename InversionTraits::GridTraits::RangeField>
      {
        using GridTraits = typename InversionTraits::GridTraits;

        enum { dim = GridTraits::dim };

        using RF     = typename GridTraits::RangeField;
        using Scalar = typename GridTraits::Scalar;
        using DF     = typename GridTraits::DomainField;

        public:

        // pattern assembly flags
        enum { doPatternVolume = true };

        // residual assembly flags
        enum { doAlphaVolume = true };

        private:

        const ModelParameters<InversionTraits,ModelType::Groundwater>& param;

        RF time;
        const int intorderadd;
        const int quadrature_factor;

        public:

        /**
         * @brief Constructor
         */
        TemporalOperator (const InversionTraits& traits, const ModelParameters<InversionTraits,ModelType::Groundwater>& param_,
            int intorderadd_ = 2, int quadrature_factor_ = 2)
          : param(param_), intorderadd(intorderadd_), quadrature_factor(quadrature_factor_)
        {}

        /**
         * @brief Volume integral depending on test and ansatz functions
         */
        template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
          void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
          {
            // get polynomial degree
            const int order    = lfsu.finiteElement().localBasis().order();
            const int intorder = intorderadd + quadrature_factor * order;

            // loop over quadrature points
            for (const auto& point : quadratureRule(eg.geometry(),intorder))
            {
              // evaluate basis functions
              std::vector<Scalar> phi(lfsu.size());
              lfsu.finiteElement().localBasis().evaluateFunction(point.position(),phi);

              // evaluate u
              RF u = 0.;
              for (unsigned int i = 0; i < lfsu.size(); i++)
                u += x(lfsu,i) * phi[i];

              const RF storativity = param.S(eg.entity(),point.position());

              // integration factor
              const RF factor = point.weight() * eg.geometry().integrationElement(point.position());

              // update residual
              if (DirectionType::isAdjoint())
              {
                for (unsigned int i = 0; i < lfsv.size(); i++)
                  r.accumulate(lfsv,i, - storativity * u * phi[i] * factor);
              }
              else
              {
                for (unsigned int i = 0; i < lfsv.size(); i++)
                  r.accumulate(lfsv,i,   storativity * u * phi[i] * factor);
              }
            }
          }

        /**
         * @brief Jacobian of volume term
         */
        template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
          void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
              M & mat) const
          {
            // get polynomial degree
            const int order    = lfsu.finiteElement().localBasis().order();
            const int intorder = intorderadd + quadrature_factor * order;

            // loop over quadrature points
            for (const auto& point : quadratureRule(eg.geometry(),intorder))
            {
              // evaluate basis functions
              std::vector<Scalar> phi(lfsu.size());
              lfsu.finiteElement().localBasis().evaluateFunction(point.position(),phi);

              const RF storativity = param.S(eg.entity(),point.position());

              // integration factor
              RF factor = point.weight() * eg.geometry().integrationElement(point.position());

              // integrate phi_j*phi_i
              if (DirectionType::isAdjoint())
              {
                for (unsigned int j = 0; j < lfsu.size(); j++)
                  for (unsigned int i = 0; i < lfsv.size(); i++)
                    mat.accumulate(lfsv,i,lfsu,j, - storativity * phi[j] * phi[i] * factor);
              }
              else
              {
                for (unsigned int j = 0; j < lfsu.size(); j++)
                  for (unsigned int i = 0; i < lfsv.size(); i++)
                    mat.accumulate(lfsv,i,lfsu,j,   storativity * phi[j] * phi[i] * factor);
              }
            }
          }

        //! set time
        void setTime (RF t)
        {
          time = t;
        }

        RF suggestTimestep(RF dt) const
        {
          // don't influence CFL condition
          return std::numeric_limits<RF>::max();
        }

      };

  }
}

#endif // DUNE_INVERSION_GROUNDWATER_OPERATOR_DG_HH
