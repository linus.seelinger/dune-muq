#ifndef DUNE_INVERSION_PARAMETERS_CONFFLOW_HH
#define DUNE_INVERSION_PARAMETERS_CONFFLOW_HH

#include<dune/inversion/wells.hh>
#include<dune/inversion/parameters.hh>

namespace Dune {
  namespace Modelling {

    /**
     * @brief Parameter class for the groundwater equation
     */
    template<typename InversionTraits>
      class ModelParameters<InversionTraits, ModelType::Groundwater>
      : public ModelParametersBase<InversionTraits>
      {
        using GridTraits = typename InversionTraits::GridTraits;

        enum {dim = GridTraits::dim};

        using RF           = typename GridTraits::RangeField;
        using Scalar       = typename GridTraits::Scalar;
        using Vector       = typename GridTraits::Vector;
        using Domain       = typename GridTraits::Domain;
        using IDomain      = typename GridTraits::IDomain;
        using Element      = typename GridTraits::Element;
        using Intersection = typename GridTraits::Intersection;

        using ParameterList   = typename InversionTraits::ParameterList;
        using ParamField      = typename ParameterList::SubParamField;
        using MeasurementList = typename InversionTraits::MeasurementList;
        using Measurements    = typename MeasurementList::SubMeasurements;

        Vector gvector;
        const Dune::ParameterTree& config;
        const Dune::Inversion::WellList<InversionTraits> wellList;
        std::shared_ptr<const SolutionStorage<InversionTraits,ModelType::Groundwater,Direction::Forward> > forwardStorage;
        std::shared_ptr<const SolutionStorage<InversionTraits,ModelType::Groundwater,Direction::Adjoint> > adjointStorage;
        std::shared_ptr<const ModelParameters<InversionTraits,ModelType::Transport> > transportParam;
        std::shared_ptr<const ParameterList>   parameterList;
        std::shared_ptr<const ParamField>      logCondField, logStorField, initialHeadField;
        std::shared_ptr<const MeasurementList> measurementList;
        std::shared_ptr<const Measurements>    measurements;

        const RF porosity, longDisp, transDisp, molecDiff;

        public:

        /**
         * @brief Constructor
         */
        ModelParameters(const InversionTraits& traits, const std::string& name)
          : ModelParametersBase<InversionTraits>(name), config(traits.config()),
          wellList(traits),
          porosity (config.get<RF>("parameters.porosity")),
          longDisp (config.get<RF>("parameters.longDispersion")),
          transDisp(config.get<RF>("parameters.transDispersion")),
          molecDiff(config.get<RF>("parameters.molecularDiffusion"))
        {
          gvector=0;
        }

        void registerModel(
            const std::string& name,
            const std::shared_ptr<const ModelParameters<InversionTraits,ModelType::Transport> >& transportParam_
            )
        {
          transportParam = transportParam_;
        }

        void registerModel(
            const std::string& name,
            const std::shared_ptr<const ModelParameters<InversionTraits,ModelType::WaterContent> >& waterContentParam_
            )
        {}

        void setStorage(
            const std::shared_ptr<const SolutionStorage<InversionTraits,ModelType::Groundwater,Direction::Forward> > storage
            )
        {
          forwardStorage = storage;
        }

        void setStorage(
            const std::shared_ptr<const SolutionStorage<InversionTraits,ModelType::Groundwater,Direction::Adjoint> > storage
            )
        {
          adjointStorage = storage;
        }

        const SolutionStorage<InversionTraits,ModelType::Groundwater,Direction::Forward>& forwardSolution() const
        {
          if (!forwardStorage)
            DUNE_THROW(Dune::Exception,"forward storage not set in GroundwaterParameter");

          return *forwardStorage;
        }

        const SolutionStorage<InversionTraits,ModelType::Groundwater,Direction::Adjoint>& adjointSolution() const
        {
          if (!adjointStorage)
            DUNE_THROW(Dune::Exception,"adjoint storage not set in GroundwaterParameter");

          return *adjointStorage;
        }

        /**
         * @brief Change the ParameterList used
         */
        void setParameterList(const std::shared_ptr<const ParameterList>& parameterList_)
        {
          parameterList    = parameterList_;
          logCondField     = parameterList->get("logCond");
          logStorField     = parameterList->get("logStor");
          initialHeadField = parameterList->get("initialHead");
        }

        /**
         * @brief Change the MeasurementList used
         */
        void setMeasurementList(const std::shared_ptr<const MeasurementList>& measurementList_)
        {
          measurementList = measurementList_;
          measurements = (*measurementList).get("head");
        }

        void setTimes(RF timeFrom, RF timeTo)
        {}

        RF timestep() const
        {
          return config.get<RF>("time.head.timestep");
        }

        RF adjointTimestep() const
        {
          return config.get<RF>("time.head.adjointTimestep",config.get<RF>("time.head.timestep"));
        }

        RF minTimestep() const
        {
          return config.get<RF>("time.head.minStep");
        }

        RF maxTimestep() const
        {
          return config.get<RF>("time.head.maxStep");
        }

        const Vector& gravity() const
        {
          return gvector;
        }

        /**
         * @brief Specific Storativity
         */
        RF S (const Element& elem, const Domain& x) const
        {
          if (!logStorField)
            DUNE_THROW(Dune::Exception,"ParamField not set in GroundwaterParameter");

          Scalar value;
          (*logStorField).evaluate(elem, x, value);

          //return std::max(1.e-10,value[0]);
          return std::exp(value[0]);
        }

        RF waterContent(const Element& elem, const Domain& x, RF h) const
        {
          return porosity + S(elem,x) * h;
        }

        RF derivWaterContent(const Element& elem, const Domain& x, RF h) const
        {
          return S(elem,x);
        }

        /**
         * @brief Absolute conductivity
         */
        RF K (const Element& elem, const Domain& x) const
        {
          if (!logCondField)
            DUNE_THROW(Dune::Exception,"ParamField not set in GroundwaterParameter");

          Scalar value;
          (*logCondField).evaluate(elem, x, value);

          return std::exp(value[0]);
        }

        RF satCond(const Element& elem, const Domain& x) const
        {
          return K(elem,x);
        }

        RF head (const Element& elem, const Domain& x, RF time) const
        {
          if (!forwardStorage)
            DUNE_THROW(Dune::Exception,"forward storage not set in GroundwaterParameter");

          Scalar headValue;
          (*forwardStorage).value(time,elem,x,headValue);

          return headValue[0];
        }

        RF cond (const Element& elem, const Domain& x, RF time) const
        {
          return K(elem,x);
        }

        RF stor (const Element& elem, const Domain& x, RF time) const
        {
          return S(elem,x);
        }

        RF derivCond (const Element& elem, const Domain& x, RF time) const
        {
          return 0.;
        }

        RF q (const Element& elem, const Domain& x, RF time) const
        {
          return wellList.flux(elem,time) / elem.geometry().volume();
        }

        /**
         * @brief Skeleton part of source term
         */
        RF q_skel (const Intersection& is, const IDomain& x, RF time) const
        {
          RF output = 0.;
          DUNE_THROW(Dune::Exception,"not up to date");

          if (measurementList->get("tracer"))
          {
            if (!transportParam)
              DUNE_THROW(Dune::Exception,"transport parameters not set in GroundwaterParameter");

            Scalar tracerValue;
            Vector adjointTracerGrad;

            (*transportParam).forwardSolution().value   (time,is.inside(),is.geometryInInside().global(x),tracerValue);
            (*transportParam).adjointSolution().gradient(time,is.inside(),is.geometryInInside().global(x),adjointTracerGrad);

            const RF K_inside  = cond(is.inside(), is.geometryInInside() .global(x),time);
            const RF K_outside = cond(is.outside(),is.geometryInOutside().global(x),time);
            /// @todo havg
            const RF K = 2./(1./(K_inside+1e-6) + 1./(K_outside+1e-6));            

            output = K * tracerValue[0] * (adjointTracerGrad * is.centerUnitOuterNormal());
          }

          return output;
        }

        RF adjTracerSource (const Element& elem, const Domain& x, RF time) const
        {
          RF output = 0.;

          if (measurementList->get("tracer"))
          {
            if (!transportParam)
              DUNE_THROW(Dune::Exception,"transport parameters not set in GroundwaterParameter");

            Scalar tracerValue;
            Vector tracerGradient;
            Scalar adjTracerTempDeriv;
            Vector adjTracerGradient;

            (*transportParam).forwardSolution().value    (time,elem,x,tracerValue);
            (*transportParam).forwardSolution().gradient (time,elem,x,tracerGradient);
            (*transportParam).adjointSolution().tempDeriv(time,elem,x,adjTracerTempDeriv);
            (*transportParam).adjointSolution().gradient (time,elem,x,adjTracerGradient);

            output = S(elem,x) * (tracerValue[0] * adjTracerTempDeriv[0] - transDisp * molecDiff * (adjTracerGradient * tracerGradient));
          }

          return output;
        }

        Vector adjTracerFlux (const Element& elem, const Domain& x, RF time) const
        {
          Vector output(0.);

          if (measurementList->get("tracer"))
          {
            if (!transportParam)
              DUNE_THROW(Dune::Exception,"transport parameters not set in GroundwaterParameter");

            Scalar tracerValue;
            Vector tracerGradient;
            Vector adjTracerGradient;
            Vector headGradient;
            Vector contrib;

            (*transportParam).forwardSolution().value   (time,elem,x,tracerValue);
            (*transportParam).forwardSolution().gradient(time,elem,x,tracerGradient);
            (*transportParam).adjointSolution().gradient(time,elem,x,adjTracerGradient);
            (*forwardStorage).gradient(time,elem,x,headGradient);

            const RF headGradNorm        = std::sqrt(headGradient * headGradient);
            const RF headTracerProd      = headGradient * tracerGradient;
            const RF headAdjTracerProd   = headGradient * adjTracerGradient;
            const RF tracerAdjTracerProd = tracerGradient * adjTracerGradient;

            contrib = adjTracerGradient;
            contrib *= (longDisp - transDisp) / headGradNorm * headTracerProd + tracerValue[0];
            output += contrib;

            contrib = tracerGradient;
            contrib *= (longDisp - transDisp) / headGradNorm * headAdjTracerProd;
            output += contrib;

            contrib = headGradient;
            contrib *= - (longDisp - transDisp) / std::pow(headGradNorm,3) * headTracerProd * headAdjTracerProd
              + transDisp / headGradNorm * tracerAdjTracerProd;
            output += contrib;
          }

          return output;
        }

        RF forwardValue (const Element& elem, const Domain& x, RF time) const
        {
          if (!forwardStorage)
            DUNE_THROW(Dune::Exception,"forward storage not set in GroundwaterParameter");

          Scalar value;
          (*forwardStorage).value(time,elem,x,value);

          return value[0];
        }

        Vector forwardFlux (const Element& elem, const Domain& x, RF time) const
        {
          if (!forwardStorage)
            DUNE_THROW(Dune::Exception,"forward storage not set in GroundwaterParameter");

          Vector flux;
          (*forwardStorage).flux(time,elem,x,flux);

          return flux;
        }

        RF adjointSource(const Element& elem, const Domain& x, RF time) const
        {
          if (measurements)
            return - measurements->deviation(elem,x) / elem.geometry().volume();
          else
            return 0.;
        }

        RF initial(const Element& elem, const Domain& x) const
        {
          if (!initialHeadField)
            DUNE_THROW(Dune::Exception,"ParamField not set in GroundwaterEquationParameter");

          Scalar value;
          (*initialHeadField).evaluate(elem, x ,value);

          return value[0];
        }

        /**
         * @brief Maximum water flux on element (for CFL condition)
         */
        template<typename Element, typename Time>
          RF maxFluxNorm(const Element& elem, const Time& time) const
          {
            RF output = 0.;
            Vector localFlux;

            Dune::GeometryType type = elem.geometry().type();
            const auto& rule = Dune::QuadratureRules
              <typename GridTraits::DomainField, GridTraits::dim>::rule(type, 1);

            // loop over quadrature points 
            for(const auto& point : rule) 
            {
              const auto& x = point.position();
              (*forwardStorage).flux(time,elem,x,localFlux);
              RF norm = 0.;
              for (const auto& entry : localFlux)
                norm += entry*entry;
              norm = std::sqrt(norm);

              output = std::max(output,norm);
            }

            return output;
          }

      };

    template<typename InversionTraits>
      class InitialValue<InversionTraits, ModelType::Groundwater, Direction::Forward>
      : public Dune::Inversion::DefaultInitial<InversionTraits,ModelParameters<InversionTraits,ModelType::Groundwater>>
      {
        public:

          using Dune::Inversion::DefaultInitial<InversionTraits,ModelParameters<InversionTraits,ModelType::Groundwater>>::DefaultInitial;
      };

    template<typename InversionTraits, typename ModelType, typename DirectionType>
      class SourceTerm;

    template<typename InversionTraits>
      class SourceTerm<InversionTraits, ModelType::Groundwater, Direction::Forward>
      : public Dune::Inversion::DefaultSource<InversionTraits,ModelParameters<InversionTraits,ModelType::Groundwater>>
      {
        public:

          using Dune::Inversion::DefaultSource<InversionTraits,ModelParameters<InversionTraits,ModelType::Groundwater>>::DefaultSource;
      };

    template<typename InversionTraits>
      class SourceTerm<InversionTraits, ModelType::Groundwater, Direction::Adjoint>
      : public Dune::Inversion::DefaultAdjointSource<InversionTraits,ModelParameters<InversionTraits,ModelType::Groundwater>>
      {
        public:

          using Dune::Inversion::DefaultAdjointSource<InversionTraits,ModelParameters<InversionTraits,ModelType::Groundwater>>::DefaultAdjointSource;
      };

  }
}

#endif // DUNE_INVERSION_PARAMETERS_CONFFLOW_HH
