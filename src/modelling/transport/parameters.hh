#ifndef DUNE_INVERSION_PARAMETERS_TRANSPORT_HH
#define DUNE_INVERSION_PARAMETERS_TRANSPORT_HH

#include<dune/inversion/parameters.hh>

namespace Dune {
  namespace Modelling {

    /**
     * @brief Parameter class for transport equation
     */
    template<typename InversionTraits>
      class ModelParameters<InversionTraits,ModelType::Transport>
      : public ModelParametersBase<InversionTraits>
      {
        using GridTraits = typename InversionTraits::GridTraits;

        enum {dim = GridTraits::dim};

        using RF           = typename GridTraits::RangeField;
        using Scalar       = typename GridTraits::Scalar;
        using Vector       = typename GridTraits::Vector;
        using Tensor       = typename GridTraits::Tensor;
        using Domain       = typename GridTraits::Domain;
        using IDomain      = typename GridTraits::IDomain;
        using Element      = typename GridTraits::Element;
        using Intersection = typename GridTraits::Intersection;

        using ParameterList   = typename InversionTraits::ParameterList;
        using ParamField      = typename ParameterList::SubParamField;
        using MeasurementList = typename InversionTraits::MeasurementList;
        using Measurements    = typename MeasurementList::SubMeasurements;

        const Dune::ParameterTree& config;
        const Dune::Inversion::WellList<InversionTraits> wellList;
        std::shared_ptr<const ModelParameters<InversionTraits,ModelType::Groundwater> > groundwaterParam;
        std::shared_ptr<const SolutionStorage<InversionTraits,ModelType::Transport,Direction::Forward> > forwardStorage;
        std::shared_ptr<const SolutionStorage<InversionTraits,ModelType::Transport,Direction::Adjoint> > adjointStorage;
        std::shared_ptr<const ParameterList>   parameterList;
        std::shared_ptr<const ParamField>      initialTracerField;
        std::shared_ptr<const MeasurementList> measurementList;
        std::shared_ptr<const Measurements>    measurements;

        RF timeUpper;
        RF cflFactor;
        RF longDisp, transDisp, molecDiff;

        public:

        /**
         * @brief Constructor
         */
        ModelParameters(const InversionTraits& traits, const std::string& name)
          : ModelParametersBase<InversionTraits>(name), config(traits.config()),
          wellList(traits),
          cflFactor (config.get<RF>  ("general.cflFactor",1.)),
          longDisp  (config.get<RF>  ("parameters.longDispersion")),
          transDisp (config.get<RF>  ("parameters.transDispersion")),
          molecDiff (config.get<RF>  ("parameters.molecularDiffusion"))
        {}

        void registerModel(
            const std::string& name,
            const std::shared_ptr<const ModelParameters<InversionTraits,ModelType::Groundwater> >& groundwaterParam_
            )
        {
          groundwaterParam = groundwaterParam_;
        }

        void setStorage(
            const std::shared_ptr<const SolutionStorage<InversionTraits,ModelType::Transport,Direction::Forward> > storage
            )
        {
          forwardStorage = storage;
        }

        void setStorage(
            const std::shared_ptr<const SolutionStorage<InversionTraits,ModelType::Transport,Direction::Adjoint> > storage
            )
        {
          adjointStorage = storage;
        }

        const SolutionStorage<InversionTraits,ModelType::Transport,Direction::Forward>& forwardSolution() const
        {
          if (!forwardStorage)
            DUNE_THROW(Dune::Exception,"forward storage not set in TransportParameter");

          return *forwardStorage;
        }

        const SolutionStorage<InversionTraits,ModelType::Transport,Direction::Adjoint>& adjointSolution() const
        {
          if (!adjointStorage)
            DUNE_THROW(Dune::Exception,"adjoint storage not set in TransportParameter");

          return *adjointStorage;
        }

        /**
         * @brief Change the ParameterList used
         */
        void setParameterList(const std::shared_ptr<const ParameterList>& parameterList_)
        {
          parameterList      = parameterList_;
          initialTracerField = (*parameterList).get("initialTracer");
        }

        /**
         * @brief Change the MeasurementList used
         */
        void setMeasurementList(const std::shared_ptr<const MeasurementList>& measurementList_)
        {
          measurementList = measurementList_;
          measurements = (*measurementList).get("tracer");
        }

        void setTimes(RF timeFrom, RF timeTo)
        {
          timeUpper = std::max(timeFrom,timeTo);
        }

        RF timestep() const
        {
          return config.get<RF>("time.tracer.timestep");
        }

        RF adjointTimestep() const
        {
          return config.get<RF>("time.tracer.adjointTimestep",config.get<RF>("time.tracer.timestep"));
        }

        RF minTimestep() const
        {
          return config.get<RF>("time.tracer.minStep");
        }

        RF maxTimestep() const
        {
          return config.get<RF>("time.tracer.maxStep");
        }

        /**
         * @brief Normal convective flux across interface
         */
        RF vNormal (const Intersection& is, const IDomain& x, RF time) const
        {
          const Domain& insideLocal = is.geometryInInside().global(x);
          const Domain& normal = is.unitOuterNormal(x);
          return (waterFlux(is.inside(),insideLocal,time) * normal)/waterContent(is.inside(),insideLocal,time);
        }

        /**
         * @brief Diffusion tensor
         */
        Tensor diffusion (const Element& elem, const Domain& x, RF time) const
        {
          // D_ij = (lambda_l - lambda_t) v_i v_j / v_norm + (lambda_t v_norm + D_s (theta)) delta_ij

          const Vector& flux = waterFlux(elem,x,time);

          RF fluxNorm = flux.two_norm();

          Tensor dispersion;
          for (unsigned int i = 0; i < dim; i++)
            for (unsigned int j = 0; j < dim; j++)
            {
              dispersion[i][j] = (longDisp - transDisp) * flux[i] * flux[j] / (fluxNorm + 1e-30);
              if (i == j)
              {
                dispersion[i][j] += transDisp * fluxNorm + waterContent(elem,x,time) * molecDiff;
              }
            }

          return dispersion;
        }

        RF molecDiffusion (const Element& elem, const Domain& x) const
        {
          return molecDiff;
        }

        RF longDispersion (const Element& elem, const Domain& x) const
        {
          return longDisp;
        }

        RF transDispersion (const Element& elem, const Domain& x) const
        {
          return transDisp;
        }

        RF forwardValue (const Element& elem, const Domain& x, RF time) const
        {
          if (!forwardStorage)
            DUNE_THROW(Dune::Exception,"forward storage not set in TransportParameter");

          Scalar value;
          (*forwardStorage).value(time,elem,x,value);

          return value[0];
        }

        Vector forwardFlux (const Element& elem, const Domain& x, RF time) const
        {
          if (!forwardStorage)
            DUNE_THROW(Dune::Exception,"forward storage not set in TransportParameter");

          Vector flux;
          (*forwardStorage).flux(time,elem,x,flux);

          return flux;
        }

        Vector waterFlux (const Element& elem, const Domain& x, RF time) const
        {
          Vector flux;
          if (groundwaterParam)
            (*groundwaterParam).forwardSolution().flux(time,elem,x,flux);
          else
            DUNE_THROW(Dune::Exception,"flow parameters not set in TransportParameter");

          return flux;
        }

        /**
         * @brief Maximum velocity in cell for CFL condition
         */
        template<typename Element, typename Time>
          RF maxVelocity(const Element& elem, const Time& time) const
          {
            if (groundwaterParam)
              return (*groundwaterParam).maxFluxNorm(elem,time)/waterContent(elem,elem.geometry().center(),time);
            else
              DUNE_THROW(Dune::Exception,"flow parameters not set in TransportParameter");
          }


        RF potential(const Element& elem, const Domain& x, RF time) const
        {
          Scalar head;
          if (groundwaterParam)
            (*groundwaterParam).forwardSolution().value(time,elem,x,head);
          else
            DUNE_THROW(Dune::Exception,"flow parameters not set in TransportParameter");

          return head[0];
        }

        RF potentialTempDeriv(const Element& elem, const Domain& x, RF time) const
        {
          Scalar headDeriv;
          if (groundwaterParam)
            (*groundwaterParam).forwardSolution().tempDeriv(time,elem,x,headDeriv);
          else
            DUNE_THROW(Dune::Exception,"flow parameters not set in TransportParameter");

          return headDeriv[0];
        }

        RF waterContent(const Element& elem, const Domain& x, RF time) const
        {
          const RF head = potential(elem,x,time);

          if (groundwaterParam)
            return groundwaterParam->waterContent(elem,x,head);
          else
            DUNE_THROW(Dune::Exception,"flow parameters not set in TransportParameter");
        }

        RF derivWaterContent(const Element& elem, const Domain& x, const RF time) const
        {
          const Scalar& head = potential(elem,x,time);

          if (groundwaterParam)
            return groundwaterParam->derivWaterContent(elem,x,head);
          else
            DUNE_THROW(Dune::Exception,"flow parameters not set in TransportParameter");
        }

        RF adjointSource(const Element& elem, const Domain& x, RF time) const
        {
          if (measurements)
            return - measurements->deviation(elem,x) / elem.geometry().volume();
          else
            return 0.;
        }

        RF initial(const Element& elem, const Domain& x) const
        {
          if (!initialTracerField)
            DUNE_THROW(Dune::Exception,"ParamField not set in TransportParameter");

          Scalar value;
          (*initialTracerField).evaluate(elem,x,value);

          return value[0];
        }

        RF pumpRate(const Element& elem, RF time) const
        {
          return wellList.flux(elem,time);
          //return 0.;
        }

      };

    template<typename InversionTraits>
      class InitialValue<InversionTraits, ModelType::Transport, Direction::Forward>
      : public Dune::Inversion::DefaultInitial<InversionTraits,ModelParameters<InversionTraits,ModelType::Transport>>
      {
        public:

          using Dune::Inversion::DefaultInitial<InversionTraits,ModelParameters<InversionTraits,ModelType::Transport>>::DefaultInitial;
      };

    /**
     * @brief Source term for the transport equation
     */
    template<typename InversionTraits>
      class SourceTerm<InversionTraits, ModelType::Transport, Direction::Forward>
      {
        using GridTraits = typename InversionTraits::GridTraits;

        using RF      = typename GridTraits::RangeField;
        using Domain  = typename GridTraits::Domain;
        using Element = typename GridTraits::Element;

        const ModelParameters<InversionTraits,ModelType::Transport>& parameters;

        public:

        SourceTerm(const Dune::ParameterTree& config, const ModelParameters<InversionTraits,ModelType::Transport>& parameters_)
          : parameters(parameters_)
        {}

        RF q_source (const Element& elem, const Domain& x, RF time) const
        {
          RF output = 0.;
          /*
             RF pumpRate = parameter.pumpRate(elem,time);

          //if (time >= 4.32e4 && time < 8.64e4)
          if (time >= 5000 && time < 15000)
          {
          if (pumpRate > 0.)
          output += pumpRate;
          }
          */

          return output / elem.geometry().volume();
        }

        RF q_sink (const Element& elem, const Domain& x, RF time) const
        {
          RF output = 0.;

          RF pumpRate = parameters.pumpRate(elem,time);
          if (pumpRate < 0.)
            output += pumpRate;

          return output / elem.geometry().volume();
        }

      };

    /**
     * @brief Source term for the transport equation
     */
    template<typename InversionTraits>
      class SourceTerm<InversionTraits, ModelType::Transport, Direction::Adjoint>
      {
        using GridTraits = typename InversionTraits::GridTraits;

        using RF      = typename GridTraits::RangeField;
        using Domain  = typename GridTraits::Domain;
        using Element = typename GridTraits::Element;

        const ModelParameters<InversionTraits,ModelType::Transport>& parameters;

        public:

        SourceTerm(const Dune::ParameterTree& config, const ModelParameters<InversionTraits,ModelType::Transport>& parameters_)
          : parameters(parameters_)
        {}

        RF q_source (const Element& elem, const Domain& x, RF time) const
        {
          return parameters.adjointSource(elem,x,time);
        }

        RF q_sink (const Element& elem, const Domain& x, RF time) const
        {
          RF output = 0.;

          RF pumpRate = parameters.pumpRate(elem,time);
          if (pumpRate > 0.)
            output -= pumpRate;

          return output / elem.geometry().volume();
        }

      };

  }
}

#endif // DUNE_INVERSION_PARAMETERS_TRANSPORT_HH
