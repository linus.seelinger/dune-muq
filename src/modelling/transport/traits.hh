#ifndef TRANSPORTTRAITS_HH
#define TRANSPORTTRAITS_HH

#include<dune/modelling/equation.hh>
#include<dune/modelling/solvers.hh>

#include<src/transport/localoperatorDG.hh>
#include<src/transport/parameters.hh>

namespace Dune {
  namespace Modelling {

    template<typename InversionTraits, typename FormulationType, typename DirectionType>
      class Equation<InversionTraits,ModelType::Transport,FormulationType,DirectionType>
      : public DifferentialEquation<InversionTraits,ModelType::Transport,FormulationType,DirectionType>
      {
        using DifferentialEquation<InversionTraits,ModelType::Transport,FormulationType,DirectionType>::DifferentialEquation;
      };

    /**
     * @brief Traits class with definitions for transport equation
     */
    template<typename InversionTraits, typename DirectionType>
      class EquationTraits<InversionTraits,ModelType::Transport,DirectionType>
      {
        public:

          using GridTraits = typename InversionTraits::GridTraits;

          using GridView    = typename GridTraits::GridView;
          using DomainField = typename GridTraits::DomainField;
          using RangeField  = typename GridTraits::RangeField;

          enum {dim = GridTraits::dim};
          enum {order = 1};

          using DiscretizationType = Discretization::DiscontinuousGalerkin;

          template<typename... T>
            using StationarySolver = StationaryLinearSolver<T...>;
          template<typename... T>
            using TransientSolver = ExplicitLinearSolver<T...>;
          //template<typename... T>
          //  using TransientSolver = ImplicitLinearSolver<T...>;

          template<typename... T>
            using FluxReconstruction = RT0Reconstruction<T...>;
          template<typename... T>
            using StorageContainer = FullContainer<T...>;
          template<typename... T>
            using TemporalInterpolation = NextTimestep<T...>;

          typedef Dune::PDELab::QkDGLocalFiniteElementMap<DomainField,RangeField,order,dim,Dune::PDELab::QkDGBasisPolynomial::l2orthonormal> FEM;
          typedef Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed,Dune::QkStuff::QkSize<order,dim>::value> VBE;
          typedef Dune::PDELab::P0ParallelConstraints CON;
          typedef Dune::PDELab::HeunParameter<RangeField> OneStepScheme;
          //typedef Dune::PDELab::ImplicitEulerParameter<RangeField> OneStepScheme;
          //typedef Dune::PDELab::ExplicitEulerParameter<RangeField> OneStepScheme;

          typedef Dune::PDELab::GridFunctionSpace<GridView,FEM,CON,VBE> GridFunctionSpace;
          typedef typename Dune::PDELab::Backend::Vector<GridFunctionSpace,RangeField> GridVector;

        private:

          const FEM fem;
          GridFunctionSpace space;

        public:

          EquationTraits(const InversionTraits& invTraits)
            : fem(), space(invTraits.grid().levelGridView(0),fem)
          {}

          const GridFunctionSpace& gfs() const
          {
            return space;
          }
      };

  }
}

#endif //TRANSPORTTRAITS_HH
