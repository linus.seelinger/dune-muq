#ifndef DUNE_INVERSION_TRANSPORT_OPERATOR_DG_HH
#define DUNE_INVERSION_TRANSPORT_OPERATOR_DG_HH

#include<dune/pdelab/common/quadraturerules.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/finiteelement/localbasiscache.hh>

#include<dune/inversion/parameters.hh>

namespace Dune {
  namespace Modelling {

    struct TransportDGMethod
    {
      enum Type { NIPG, SIPG, IIPG };
    };

    struct TransportDGWeights
    {
      enum Type { weightsOn, weightsOff };
    };

    /**
     * @brief DG local operator for the spatial part of the convection-diffusion equation
     *
     * \f{align*}{
     *   \nabla\cdot(-A(x) \nabla u + b(x) u) + c(x)u &=& f \mbox{ in } \Omega,  \\
     *                                              u &=& g \mbox{ on } \partial\Omega_D \\
     *                (b(x) u - A(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_N \\
     *                        -(A(x)\nabla u) \cdot n &=& o \mbox{ on } \partial\Omega_O
     * \f}
     * Note:
     *  - This formulation is valid for velocity fields which are non-divergence free.
     *  - Outflow boundary conditions should only be set on the outflow boundary
     */
    template<typename InversionTraits, typename DirectionType>
      class SpatialOperator<InversionTraits, ModelType::Transport, Discretization::DiscontinuousGalerkin, DirectionType>
      : public Dune::PDELab::NumericalJacobianVolume<SpatialOperator<InversionTraits, ModelType::Transport, Discretization::DiscontinuousGalerkin, DirectionType>>,
      public Dune::PDELab::NumericalJacobianSkeleton<SpatialOperator<InversionTraits, ModelType::Transport, Discretization::DiscontinuousGalerkin, DirectionType>>,
      public Dune::PDELab::NumericalJacobianBoundary<SpatialOperator<InversionTraits, ModelType::Transport, Discretization::DiscontinuousGalerkin, DirectionType>>,
      public Dune::PDELab::FullSkeletonPattern,
      public Dune::PDELab::FullVolumePattern,
      public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<typename InversionTraits::GridTraits::RangeField>
      {
        using GridTraits = typename InversionTraits::GridTraits;

        enum { dim = GridTraits::dim };

        using RF       = typename GridTraits::RangeField;
        using Scalar   = typename GridTraits::Scalar;
        using Vector   = typename GridTraits::Vector;
        using Tensor   = typename GridTraits::Tensor;
        using DF       = typename GridTraits::DomainField;
        using Domain   = typename GridTraits::Domain;
        using IDomain  = typename GridTraits::IDomain;
        using Element  = typename GridTraits::Element;

        using FEM            = typename EquationTraits<InversionTraits,ModelType::Transport,DirectionType>::FEM;
        using LocalBasisType = typename FEM::Traits::FiniteElementType::Traits::LocalBasisType;
        using Cache          = Dune::PDELab::LocalBasisCache<LocalBasisType>;

        public:

        // pattern assembly flags
        enum { doPatternVolume = true };
        enum { doPatternSkeleton = true };

        // residual assembly flags
        enum { doAlphaVolume  = true };
        enum { doAlphaSkeleton  = true };
        enum { doAlphaBoundary  = true };

        const InversionTraits& traits;
        const ModelParameters<InversionTraits,ModelType::Transport>& param;
        const Boundary       <InversionTraits,ModelType::Transport,DirectionType> boundary;
        const SourceTerm     <InversionTraits,ModelType::Transport,DirectionType> sourceTerm;

        RF time;
        TransportDGMethod::Type method;
        TransportDGWeights::Type weights;
        RF penalty_factor;
        int intorderadd;
        int quadrature_factor;
        RF theta;

        // In theory it is possible that one and the same local operator is
        // called first with a finite element of one type and later with a
        // finite element of another type.  Since finite elements of different
        // type will usually produce different results for the same local
        // coordinate they cannot share a cache.  Here we use a vector of caches
        // to allow for different orders of the shape functions, which should be
        // enough to support p-adaptivity.  (Another likely candidate would be
        // differing geometry types, i.e. hybrid meshes.)

        std::vector<Cache> cache;

        mutable bool firstStage;
        mutable RF   dtmin;
        mutable RF   totalMass;

        // constructor

        /**
         * @brief Constructor
         */
        SpatialOperator (const InversionTraits& traits_, const ModelParameters<InversionTraits,ModelType::Transport>& param_,
            //TransportDGMethod::Type method_ = TransportDGMethod::NIPG,
            TransportDGMethod::Type method_ = TransportDGMethod::SIPG,
            //TransportDGMethod::Type method_ = TransportDGMethod::IIPG,
            //TransportDGWeights::Type weights_ = TransportDGWeights::weightsOff,
            TransportDGWeights::Type weights_ = TransportDGWeights::weightsOn)
          : traits(traits_), param(param_), boundary(traits,param.name()), sourceTerm(traits.config(),param), method(method_), weights(weights_),
          penalty_factor   (traits.config().template get<RF>("dg.penalty_factor")),
          intorderadd      (traits.config().template get<RF>("dg.intorderadd")),
          quadrature_factor(traits.config().template get<RF>("dg.quadrature_factor")),
          cache(20)
        {
          theta = 1.;
          if (method == TransportDGMethod::SIPG) theta = -1.;
          if (method == TransportDGMethod::IIPG) theta = 0.;
        }

        /**
         * @brief Volume integral depending on test and ansatz functions
         */
        template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
          void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
          {
            const int order = lfsu.finiteElement().localBasis().order();
            const int intorder = intorderadd + quadrature_factor * order;

            // evaluate diffusion tensor at cell center, assume it is constant over elements
            const Domain localcenter = referenceElement(eg.geometry()).position(0,0);
            const Tensor A = param.diffusion(eg.entity(),localcenter,time);

            Dune::DiagonalMatrix<DF,dim> jac;
            std::vector<Vector> gradphi(lfsu.size());
            Vector gradu(0.);
            Vector Agradu(0.);

            // loop over quadrature points
            for (const auto& point : quadratureRule(eg.geometry(),intorder))
            {
              // evaluate basis functions
              const std::vector<Scalar>& phi = cache[order].evaluateFunction(point.position(),lfsu.finiteElement().localBasis());

              // evaluate u
              RF u = 0.;
              for (unsigned int i = 0; i < lfsu.size(); i++)
                u += x(lfsu,i) * phi[i];

              // evaluate gradient of basis functions (we assume Galerkin method lfsu = lfsv)
              using JacobianType = typename LFSU::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType;
              const std::vector<JacobianType>& js = cache[order].evaluateJacobian(point.position(),lfsu.finiteElement().localBasis());

              // transform gradients to real element
              jac = eg.geometry().jacobianInverseTransposed(point.position());
              for (unsigned int i = 0; i < lfsu.size(); i++)
                jac.mv(js[i][0],gradphi[i]);

              // compute gradient of u
              gradu = 0.;
              for (unsigned int i = 0; i < lfsu.size(); i++)
                gradu.axpy(x(lfsu,i),gradphi[i]);

              // compute A * gradient of u
              A.mv(gradu,Agradu);

              // evaluate velocity field
              Vector b = param.waterFlux(eg.entity(),point.position(),time);
              if (DirectionType::isAdjoint())
                b *= -1.;

              // evaluate source term
              RF q_source = sourceTerm.q_source(eg.entity(),point.position(),time);

              // evaluate sink term (reaction term)
              RF q_sink   = sourceTerm.q_sink(eg.entity(),point.position(),time);

              // integration factor
              const RF factor = point.weight() * eg.geometry().integrationElement(point.position());

              // update residual
              for (unsigned int i = 0; i < lfsv.size(); i++)
              {
                r.accumulate(lfsv,i,( Agradu*gradphi[i] - u*(b*gradphi[i]) ) * factor);
                r.accumulate(lfsv,i,( (- q_sink * u - q_source) * phi[i]) * factor);
              }

              // contribute to mass computation
              if (firstStage)
                totalMass += u * factor;
            }
          }

        /**
         * @brief Skeleton integral depending on test and ansatz functions
         */
        // each face is only visited ONCE!
        template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
          void alpha_skeleton (const IG& ig,
              const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
              const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
              R& r_s, R& r_n) const
          {
            // geometry information
            const RF faceVolume    = ig          .geometry().volume();
            const RF insideVolume  = ig.inside() .geometry().volume();
            const RF outsideVolume = ig.outside().geometry().volume();

            // contribute to timestep calculation
            if (firstStage)
            {
              const RF distance = havg(insideVolume,outsideVolume)/faceVolume;

              // compute maximum local velocity |v|
              const RF insideMaxVelocity  = param.maxVelocity(ig.inside() ,time);
              const RF outsideMaxVelocity = param.maxVelocity(ig.outside(),time);

              const RF maxVelocity = std::max(insideMaxVelocity,outsideMaxVelocity);

              // update minimum of admissable timesteps
              dtmin = std::min(dtmin,distance/maxVelocity);
            }

            // get polynomial degree
            const int order_s  = lfsu_s.finiteElement().localBasis().order();
            const int order_n  = lfsu_n.finiteElement().localBasis().order();
            const int degree   = std::max( order_s, order_n );
            const int intorder = intorderadd + quadrature_factor * degree;

            // evaluate permeability tensors
            const Domain&
              local_s = referenceElement(ig.inside().geometry()).position(0,0);
            const Domain&
              local_n = referenceElement(ig.outside().geometry()).position(0,0);
            const Tensor A_s = param.diffusion(ig.inside(),local_s,time);
            const Tensor A_n = param.diffusion(ig.outside(),local_n,time);

            // tensor times normal
            const Domain& n_F = ig.centerUnitOuterNormal();
            Vector An_F_s;
            A_s.mv(n_F,An_F_s);
            Vector An_F_n;
            A_n.mv(n_F,An_F_n);

            // compute weights
            RF omega_s = 0.5, omega_n = 0.5, harmonic_average = 1.;
            if (weights == TransportDGWeights::weightsOn)
            {
              RF delta_s = (An_F_s*n_F);
              RF delta_n = (An_F_n*n_F);
              omega_s = delta_n/(delta_s+delta_n+1e-20);
              omega_n = delta_s/(delta_s+delta_n+1e-20);
              harmonic_average = 2. * delta_s*delta_n/(delta_s+delta_n+1e-20);
            }

            // penalty factor
            const RF h_F = std::min(insideVolume,outsideVolume)/faceVolume;
            RF penalty = (penalty_factor/h_F) * harmonic_average * degree*(degree+dim-1);

            Dune::DiagonalMatrix<DF,dim> jac;
            std::vector<Vector> tgradphi_s(lfsu_s.size());
            std::vector<Vector> tgradphi_n(lfsu_n.size());
            Vector gradu_s(0.);
            Vector gradu_n(0.);

            // loop over quadrature points and integrate normal flux
            for (const auto& point : quadratureRule(ig.geometry(),intorder))
            {
              // position of quadrature point in local coordinates of elements
              const Domain local_s = ig.geometryInInside().global(point.position());
              const Domain local_n = ig.geometryInOutside().global(point.position());

              // evaluate basis functions
              const std::vector<Scalar>& phi_s = cache[order_s].evaluateFunction(local_s,lfsu_s.finiteElement().localBasis());
              const std::vector<Scalar>& phi_n = cache[order_n].evaluateFunction(local_n,lfsu_n.finiteElement().localBasis());

              // evaluate u
              RF u_s = 0., u_n = 0.;
              for (unsigned int i = 0; i < lfsu_s.size(); i++)
                u_s += x_s(lfsu_s,i) * phi_s[i];
              for (unsigned int i = 0; i < lfsu_n.size(); i++)
                u_n += x_n(lfsu_n,i) * phi_n[i];

              // evaluate gradient of basis functions (we assume Galerkin method lfsu = lfsv)
              using JacobianType = typename LFSU::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType;
              const std::vector<JacobianType>& gradphi_s = cache[order_s].evaluateJacobian(local_s,lfsu_s.finiteElement().localBasis());
              const std::vector<JacobianType>& gradphi_n = cache[order_n].evaluateJacobian(local_n,lfsu_n.finiteElement().localBasis());

              // transform gradients to real element
              jac = ig.inside().geometry().jacobianInverseTransposed(local_s);
              for (unsigned int i = 0; i < lfsu_s.size(); i++)
                jac.mv(gradphi_s[i][0],tgradphi_s[i]);
              jac = ig.outside().geometry().jacobianInverseTransposed(local_n);
              for (unsigned int i = 0; i < lfsu_n.size(); i++)
                jac.mv(gradphi_n[i][0],tgradphi_n[i]);

              // compute gradient of u
              gradu_s = 0.;
              for (unsigned int i = 0; i < lfsu_s.size(); i++)
                gradu_s.axpy(x_s(lfsu_s,i),tgradphi_s[i]);
              gradu_n = 0.;
              for (unsigned int i = 0; i < lfsu_n.size(); i++)
                gradu_n.axpy(x_n(lfsu_n,i),tgradphi_n[i]);

              // evaluate velocity field and upwinding, assume H(div) velocity field => may choose any side
              Vector b = param.waterFlux(ig.inside(),local_s,time);
              if (DirectionType::isAdjoint())
                b *= -1.;

              // face normal vector
              const Domain normal = ig.unitOuterNormal(point.position());

              const RF normal_flux = b * normal;

              RF u_upwind;
              if (normal_flux >= 0.)
                u_upwind = u_s;
              else
                u_upwind = u_n;

              // integration factor
              const RF factor = point.weight() * ig.geometry().integrationElement(point.position());

              // compute jump in solution
              const RF jump = u_s - u_n;

              // additional
              // convection term
              const RF convective_flux = u_upwind * normal_flux * factor;
              for (unsigned int i = 0; i < lfsv_s.size(); i++)
                r_s.accumulate(lfsv_s,i,   convective_flux * phi_s[i]);
              for (unsigned int i = 0; i < lfsv_n.size(); i++)
                r_n.accumulate(lfsv_n,i, - convective_flux * phi_n[i]);

              // update residual (flux term)
              // diffusion term
              // consistency term
              RF diffusive_flux = - (omega_s * (An_F_s * gradu_s) + omega_n * (An_F_n * gradu_n)) * factor;
              for (unsigned int i = 0; i < lfsv_s.size(); i++)
                r_s.accumulate(lfsv_s,i,   diffusive_flux * phi_s[i]);
              for (unsigned int i = 0; i < lfsv_n.size(); i++)
                r_n.accumulate(lfsv_n,i, - diffusive_flux * phi_n[i]);

              // update residual (symmetry term)
              // (non-)symmetric IP term
              // symmetry term
              for (unsigned int i = 0; i < lfsv_s.size(); i++)
                r_s.accumulate(lfsv_s,i, theta * omega_s * (An_F_s * tgradphi_s[i]) * jump * factor);
              for (unsigned int i = 0; i < lfsv_n.size(); i++)
                r_n.accumulate(lfsv_n,i, theta * omega_n * (An_F_n * tgradphi_n[i]) * jump * factor);

              // standard IP term integral
              // penalty term
              for (unsigned int i = 0; i < lfsv_s.size(); i++)
                r_s.accumulate(lfsv_s,i,   penalty * jump * phi_s[i] * factor);
              for (unsigned int i = 0; i < lfsv_n.size(); i++)
                r_n.accumulate(lfsv_n,i, - penalty * jump * phi_n[i] * factor);
            }
          }

        /**
         * @brief Boundary integral depending on test and ansatz functions
         */
        template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
          void alpha_boundary (const IG& ig,
              const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
              R& r_s) const
          {
            // geometry information
            const RF faceVolume   = ig         .geometry().volume();
            const RF insideVolume = ig.inside().geometry().volume();

            // contribute to timestep calculation
            if (firstStage)
            {

              const RF distance = insideVolume/faceVolume;

              // compute maximum local velocity |v|
              const RF maxVelocity = param.maxVelocity(ig.inside(),time);

              // update minimum of admissable timesteps
              dtmin = std::min(dtmin,distance/maxVelocity);
            }

            // get polynomial degree
            const int order_s  = lfsu_s.finiteElement().localBasis().order();
            const int degree   = order_s;
            const int intorder = intorderadd + quadrature_factor * degree;

            // evaluate permeability tensors
            const Domain&
              local_s = referenceElement(ig.inside().geometry()).position(0,0);
            const Tensor A_s = param.diffusion(ig.inside(),local_s,time);

            // compute weights
            const Domain& n_F = ig.centerUnitOuterNormal();
            Vector An_F_s;
            A_s.mv(n_F,An_F_s);
            RF harmonic_average;
            if (weights == TransportDGWeights::weightsOn)
              harmonic_average = An_F_s*n_F;
            else
              harmonic_average = 1.;

            // penalty factor
            const RF h_F = insideVolume/faceVolume;
            const RF penalty = (penalty_factor/h_F) * harmonic_average * degree*(degree+dim-1);

            Dune::DiagonalMatrix<DF,dim> jac;
            std::vector<Vector> tgradphi_s(lfsu_s.size());
            Vector gradu_s(0.);

            // loop over quadrature points and integrate normal flux
            for (const auto& point : quadratureRule(ig.geometry(),intorder))
            {
              // position of quadrature point in local coordinates of elements
              const Domain local_s = ig.geometryInInside().global(point.position());

              // evaluate boundary condition
              BoundaryCondition::Type bc = boundary.bc(ig.intersection(),point.position(),time);

              // evaluate basis functions
              const std::vector<Scalar>& phi_s = cache[order_s].evaluateFunction(local_s,lfsu_s.finiteElement().localBasis());

              // integration factor
              const RF factor = point.weight() * ig.geometry().integrationElement(point.position());

              if (BoundaryCondition::isNeumann(bc))
              {
                // evaluate flux boundary condition
                const RF normal_flux = boundary.j(ig.intersection(),point.position(),time) * factor;

                // update residual (flux term)
                for (unsigned int i = 0; i < lfsv_s.size(); i++)
                  r_s.accumulate(lfsv_s,i, normal_flux * phi_s[i]);

                continue;
              }

              // evaluate u
              RF u_s = 0.;
              for (unsigned int i = 0; i < lfsu_s.size(); i++)
                u_s += x_s(lfsu_s,i) * phi_s[i];

              // evaluate velocity field and upwinding, assume H(div) velocity field => choose any side
              Vector b = param.waterFlux(ig.inside(),local_s,time);
              if (DirectionType::isAdjoint())
                b *= -1.;

              // face normal vector
              const Domain& normal = ig.unitOuterNormal(point.position());

              RF normal_flux= b * normal;

              if ((!DirectionType::isAdjoint() && BoundaryCondition::isOutflow(bc)) || (DirectionType::isAdjoint() && BoundaryCondition::isDirichlet(bc)))
              {
                //if (normal_flux<-1e-10)
                //std::cout << "Outflow boundary condition on inflow! " << normal_flux << " " << ig.inside().geometry().global(local_s)[0] << " " << ig.inside().geometry().global(local_s)[1] << std::endl;
                //DUNE_THROW(Dune::Exception,"Outflow boundary condition on inflow!");

                normal_flux = std::max(normal_flux,0.);

                // convection term
                const RF convective_flux = u_s * normal_flux * factor;
                for (unsigned int i = 0; i < lfsv_s.size(); i++)
                  r_s.accumulate(lfsv_s,i, convective_flux * phi_s[i]);

                continue;
              }

              // jump relative to Dirichlet value
              const RF g = boundary.g(ig.intersection(),point.position(),time);
              const RF jump = u_s - g;

              // evaluate gradient of basis functions (we assume Galerkin method lfsu = lfsv)
              using JacobianType = typename LFSV::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType;
              const std::vector<JacobianType>& gradphi_s = cache[order_s].evaluateJacobian(local_s,lfsu_s.finiteElement().localBasis());

              // transform gradients to real element
              jac = ig.inside().geometry().jacobianInverseTransposed(local_s);
              for (unsigned int i = 0; i < lfsu_s.size(); i++)
                jac.mv(gradphi_s[i][0],tgradphi_s[i]);

              // compute gradient of u
              gradu_s = 0.;
              for (unsigned int i = 0; i < lfsu_s.size(); i++)
                gradu_s.axpy(x_s(lfsu_s,i),tgradphi_s[i]);

              // upwind
              RF u_upwind;
              if (normal_flux >= 0.)
                u_upwind = u_s;
              else
                u_upwind = g;

              // additional
              // convection term
              const RF convective_flux = u_upwind * normal_flux * factor;
              for (unsigned int i = 0; i < lfsv_s.size(); i++)
                r_s.accumulate(lfsv_s,i, convective_flux * phi_s[i]);

              // update residual (flux term)
              // diffusion term
              // consistency term
              const RF diffusive_flux = - (An_F_s * gradu_s) * factor;
              for (unsigned int i = 0; i < lfsv_s.size(); i++)
                r_s.accumulate(lfsv_s,i, diffusive_flux * phi_s[i]);

              // update residual (symmetry term)
              // (non-)symmetric IP term
              // symmetry term
              for (unsigned int i = 0; i < lfsv_s.size(); i++)
                //r_s.accumulate(lfsv_s,i, theta * 0.5 * (An_F_s * tgradphi_s[i]) * jump * factor);
                r_s.accumulate(lfsv_s,i, theta * (An_F_s * tgradphi_s[i]) * jump * factor);


              // standard IP term
              // penalty term
              for (unsigned int i = 0; i < lfsv_s.size(); i++)
                r_s.accumulate(lfsv_s,i, penalty * jump * phi_s[i] * factor);
            }
          }

        /**
         * @brief Set up CFL timestep calculation in first stage
         */
        void preStage(RF time, int r)
        {
          if (r == 1)
          {
            firstStage = true;
            dtmin = std::numeric_limits<RF>::max();
            totalMass = 0.;
          }
          else
            firstStage = false;
        }

      //! set time
      void setTime (RF t)
      {
        time = t;
      }

      template<typename Intersection, typename ScalarDGF, typename LocalGradDGF>
        RF normalFluxDeriv(const Intersection& intersection, const IDomain& x, const ScalarDGF& scalar, const LocalGradDGF& localGradient, RF time) const
        {
          DUNE_THROW(Dune::NotImplemented,
              "derivative of normal flux not implemented for groundwater flow equation");
        }

      template<typename Intersection, typename ScalarDGF, typename LocalGradDGF>
        RF normalFlux(const Intersection& intersection, const IDomain& x, const ScalarDGF& scalar, const LocalGradDGF& localGradient, RF time) const
        {
          const Domain& xInside = intersection.geometryInInside().global(x);
          Scalar innerValue;
          (*scalar).evaluate(intersection.inside(),xInside,innerValue);
          Vector innerGrad;
          (*localGradient).evaluate(intersection.inside(),xInside,innerGrad);

          if (intersection.neighbor())
          {
            const Domain& xOutside = intersection.geometryInOutside().global(x);
            Scalar outerValue;
            (*scalar).evaluate(intersection.outside(),xOutside,outerValue);
            Vector outerGrad;
            (*localGradient).evaluate(intersection.outside(),xOutside,outerGrad);

            return skeletonNormalFlux(intersection,x,innerValue[0],outerValue[0],innerGrad,outerGrad,time);
          }
          else if (intersection.boundary())
          {
            // evaluate boundary condition type
            Dune::Modelling::BoundaryCondition::Type bc = boundary.bc(intersection,x,time);

            if (Dune::Modelling::BoundaryCondition::isDirichlet(bc))
              return dirichletNormalFlux(intersection,x,innerValue[0],innerGrad,time);
            else if (Dune::Modelling::BoundaryCondition::isNeumann(bc))
              return boundary.j(intersection,x,time);
            else
              DUNE_THROW(Dune::Exception,"unknown boundary condition type");
          }
          else
            return 0.;
        }

      template<typename Intersection, typename ScalarDGF, typename LocalGradDGF>
        RF normalDerivative(const Intersection& intersection, const IDomain& x, const ScalarDGF& scalar, const LocalGradDGF& localGradient, RF time) const
        {
          const Domain& xInside  = intersection.geometryInInside().global(x);
          Scalar innerValue;
          (*scalar).evaluate(intersection.inside(),xInside,innerValue);
          Vector innerGrad;
          (*localGradient).evaluate(intersection.inside(),xInside,innerGrad);

          if (intersection.neighbor())
          {
            const Domain& xOutside = intersection.geometryInOutside().global(x);
            Scalar outerValue;
            (*scalar).evaluate(intersection.outside(),xOutside,outerValue);
            Vector outerGrad;
            (*localGradient).evaluate(intersection.outside(),xOutside,outerGrad);

            return skeletonNormalDerivative(intersection,x,innerValue[0],outerValue[0],innerGrad,outerGrad);
          }
          else if (intersection.boundary())
          {
            // evaluate boundary condition type
            Dune::Modelling::BoundaryCondition::Type bc = boundary.bc(intersection,x,time);

            if (Dune::Modelling::BoundaryCondition::isDirichlet(bc))
              return dirichletNormalDerivative(intersection,x,innerValue[0],innerGrad,time);
            else if (Dune::Modelling::BoundaryCondition::isNeumann(bc))
            {
              const Domain& xInside = intersection.geometryInInside().global(x);
              const RF boundaryFlux = boundary.j(intersection,x,time);
              const Tensor& D_inside = param.diffusion(intersection.inside(),xInside,time);
              const Domain& normal = intersection.unitOuterNormal(x);
              const RF Dn_inside = normal * FMatrixHelp::mult(D_inside,normal);
              Scalar innerValue;
              (*scalar).evaluate(intersection.inside(), xInside,innerValue);
              RF v = param.vNormal(intersection,x,time);
              if (DirectionType::isAdjoint())
                v *= -1.;
              // revert flux computation to obtain gradient on boundary
              return - (boundaryFlux - v * innerValue[0]) / Dn_inside;
            }
            else
              DUNE_THROW(Dune::Exception,"unknown boundary condition type");
          }
          else
            return 0.;
        }

      /**
       * @brief Suggest timestep based on CFL condition
       */
      RF suggestTimestep(RF dt) const
      {
        std::cout << "total mass: " << totalMass << std::endl;

        std::cout << "dt: " << dt << " dtmin: " << dtmin << std::endl;
        if (dt*dtmin > 0.)
        {
          std::cout << "same sign: min(" << dt << ", " << dtmin << ")" << std::endl;
          return std::min(dt,  traits.grid().comm().min(dtmin));
        }
        else
        {
          std::cout << "opp. sign: max(" << dt << ", " << - traits.grid().comm().min(dtmin) << ")" << std::endl;
          std::cout << "opp. sign: " << std::max(dt,- traits.grid().comm().min(dtmin)) << std::endl;
          return std::max(dt,- traits.grid().comm().min(dtmin));
        }
      }

        private:

      /**
       * @brief Flux in normal direction across interface
       */
      template<typename Intersection>
        RF skeletonNormalFlux(const Intersection& intersection, const IDomain& x, RF innerValue, RF outerValue, const Vector& innerGrad, const Vector& outerGrad, RF time) const
        {
          // geometry information
          const Domain& xInside  = intersection.geometryInInside() .global(x);
          const Domain& xOutside = intersection.geometryInOutside().global(x);

          const RF faceVolume    = intersection          .geometry().volume();
          const RF insideVolume  = intersection.inside() .geometry().volume();
          const RF outsideVolume = intersection.outside().geometry().volume();

          const Domain& normal = intersection.unitOuterNormal(x);

          // advection velocity
          RF v = param.vNormal(intersection,x,time);
          if (DirectionType::isAdjoint())
            v *= -1.;

          // upwinding
          const RF upwindValue = (v >= 0) ? innerValue : outerValue;

          // conductivity
          const Tensor& D_inside  = param.diffusion(intersection.inside(), xInside, time);
          const Tensor& D_outside = param.diffusion(intersection.outside(),xOutside,time);
          //const RF Dn_inside  = normal * FMatrixHelp::mult(D_inside,normal);
          //const RF Dn_outside = normal * FMatrixHelp::mult(D_outside,normal);
          const Vector& Dn_inside  = FMatrixHelp::mult(D_inside,normal);
          const Vector& Dn_outside = FMatrixHelp::mult(D_outside,normal);
          //const RF D = havg(Dn_inside,Dn_outside);
          const RF nDn_inside  = normal * Dn_inside;
          const RF nDn_outside = normal * Dn_outside;
          const RF D = havg(nDn_inside,nDn_outside);
          const RF omega_inside  = nDn_outside/(nDn_inside+nDn_outside+1e-20);
          const RF omega_outside = nDn_inside /(nDn_inside+nDn_outside+1e-20);

          // get polynomial degree
          //const int order_s  = lfsu_s.finiteElement().localBasis().order();
          //const int order_n  = lfsu_n.finiteElement().localBasis().order();
          //const int degree   = std::max( order_s, order_n );
          const int degree = 1;

          // penalty factor
          const RF h_F = std::min(insideVolume,outsideVolume)/faceVolume;
          RF penalty = (penalty_factor/h_F) * degree*(degree+dim-1);

          // compute jump in solution
          const RF jump = innerValue - outerValue;

          //return v * upwindValue - D * (omega_inside * (innerGrad*normal) + omega_outside * (outerGrad*normal) - penalty * jump);
          return v * upwindValue - (omega_inside * (innerGrad * Dn_inside) + omega_outside * (outerGrad * Dn_outside) - penalty * D * jump);
        }

      /**
       * @brief Derivative of solution in normal direction across interface
       */
      template<typename Intersection>
        RF skeletonNormalDerivative(const Intersection& intersection, const IDomain& x, RF innerValue, RF outerValue, const Vector& innerGrad, const Vector& outerGrad) const
        {
          // geometry information
          const RF faceVolume    = intersection          .geometry().volume();
          const RF insideVolume  = intersection.inside() .geometry().volume();
          const RF outsideVolume = intersection.outside().geometry().volume();

          const Domain& normal = intersection.unitOuterNormal(x);

          // get polynomial degree
          //const int order_s  = lfsu_s.finiteElement().localBasis().order();
          //const int order_n  = lfsu_n.finiteElement().localBasis().order();
          //const int degree   = std::max( order_s, order_n );
          const int degree = 1;

          // penalty factor
          const RF h_F = std::min(insideVolume,outsideVolume)/faceVolume;
          RF penalty = (penalty_factor/h_F) * degree*(degree+dim-1);
          //RF penalty = 1.;

          // compute jump in solution
          const RF jump = innerValue - outerValue;

          return 0.5 * (innerGrad * normal + outerGrad * normal) - penalty * jump;
        }

      /**
       * @brief Flux in normal direction on Dirichlet boundary
       */
      template<typename Intersection>
        RF dirichletNormalFlux(const Intersection& intersection, const IDomain& x, RF innerValue, const Vector& innerGrad, RF time) const
        {
          // geometry information
          const Domain& xInside = intersection.geometryInInside().global(x);

          const RF faceVolume    = intersection          .geometry().volume();
          const RF insideVolume  = intersection.inside() .geometry().volume();
          const RF outsideVolume = intersection.outside().geometry().volume();

          const Domain& normal = intersection.unitOuterNormal(x);

          // advection velocity
          RF v = param.vNormal(intersection,x,time);
          if (DirectionType::isAdjoint())
            v *= -1.;

          // upwinding
          const RF boundaryValue = boundary.g(intersection,x,time);
          const RF upwindValue = (v >= 0) ? innerValue : boundaryValue;

          // convductivity
          const Tensor& D_inside = param.diffusion(intersection.inside(),xInside,time);
          //const RF Dn_inside = normal * FMatrixHelp::mult(D_inside,normal);
          const Vector& Dn_inside = FMatrixHelp::mult(D_inside,normal);

          // get polynomial degree
          //const int order_s  = lfsu_s.finiteElement().localBasis().order();
          //const int order_n  = lfsu_n.finiteElement().localBasis().order();
          //const int degree   = std::max( order_s, order_n );
          const int degree = 1;

          // penalty factor
          const RF h_F = std::min(insideVolume,outsideVolume)/faceVolume;
          RF penalty = (penalty_factor/h_F) * degree*(degree+dim-1);

          // compute jump in solution
          const RF jump = innerValue - boundaryValue;

          //return v * upwindValue - Dn_inside * dirichletNormalDerivative(intersection,x,innerValue,time);
          //return v * upwindValue - Dn_inside * (innerGrad * normal - penalty * jump);
          return v * upwindValue - (innerGrad * Dn_inside - ((v >= 0) ? 0 : penalty * (normal * Dn_inside) * jump));
        }

      /**
       * @brief Derivative of solution in normal direction on Dirichlet boundary
       */
      template<typename Intersection>
        RF dirichletNormalDerivative(const Intersection& intersection, const IDomain& x, RF innerValue, const Vector& innerGrad, RF time) const
        {
          // geometry information
          const RF faceVolume    = intersection          .geometry().volume();
          const RF insideVolume  = intersection.inside() .geometry().volume();
          const RF outsideVolume = intersection.outside().geometry().volume();

          const Domain& normal = intersection.unitOuterNormal(x);

          // get polynomial degree
          //const int order_s  = lfsu_s.finiteElement().localBasis().order();
          //const int order_n  = lfsu_n.finiteElement().localBasis().order();
          //const int degree   = std::max( order_s, order_n );
          const int degree = 1;

          // penalty factor
          const RF h_F = std::min(insideVolume,outsideVolume)/faceVolume;
          RF penalty = (penalty_factor/h_F) * degree*(degree+dim-1);
          //RF penalty = 1.;

          // compute jump in solution
          const RF jump = innerValue - boundary.g(intersection,x,time);

          // advection velocity
          RF v = param.vNormal(intersection,x,time);
          if (DirectionType::isAdjoint())
            v *= -1.;

          return innerGrad * normal - ((v >= 0) ? 0 : penalty * jump);
        }

      template<typename T>
        T havg (T a, T b) const
        {
          T eps = 1e-30;
          return 2./(1./(a+eps) + 1./(b+eps));
        }

      };

    /**
     * @brief DG local operator for the temporal derivative of the convection-diffusion equation
     */
    template<typename InversionTraits, typename DirectionType>
      class TemporalOperator<InversionTraits, ModelType::Transport, Discretization::DiscontinuousGalerkin, DirectionType>
      : public Dune::PDELab::FullVolumePattern,
      public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<typename InversionTraits::GridTraits::RangeField>
      {
        using GridTraits = typename InversionTraits::GridTraits;

        enum { dim = GridTraits::dim };

        using RF     = typename GridTraits::RangeField;
        using Scalar = typename GridTraits::Scalar;
        using DF     = typename GridTraits::DomainField;

        public:

        // pattern assembly flags
        enum { doPatternVolume = true };

        // residual assembly flags
        enum { doAlphaVolume = true };

        private:

        const ModelParameters<InversionTraits,ModelType::Transport>& param;

        RF time;
        const int intorderadd;
        const int quadrature_factor;

        public:

        /**
         * @brief Constructor
         */
        TemporalOperator (const InversionTraits& traits, const ModelParameters<InversionTraits,ModelType::Transport>& param_,
            int intorderadd_ = 2, int quadrature_factor_ = 2)
          : param(param_), intorderadd(intorderadd_), quadrature_factor(quadrature_factor_)
        {}

        /**
         * @brief Volume integral depending on test and ansatz functions
         */
        template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
          void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
          {
            // get polynomial degree
            const int order    = lfsu.finiteElement().localBasis().order();
            const int intorder = intorderadd + quadrature_factor * order;

            // loop over quadrature points
            for (const auto& point : quadratureRule(eg.geometry(),intorder))
            {
              // evaluate basis functions
              std::vector<Scalar> phi(lfsu.size());
              lfsu.finiteElement().localBasis().evaluateFunction(point.position(),phi);

              // evaluate u
              RF u = 0.;
              for (unsigned int i = 0; i < lfsu.size(); i++)
                u += x(lfsu,i) * phi[i];

              const RF waterC = param.waterContent(eg.entity(),point.position(),time);

              // integration factor
              const RF factor = point.weight() * eg.geometry().integrationElement(point.position());

              // update residual
              if (DirectionType::isAdjoint())
              {
                for (unsigned int i = 0; i < lfsv.size(); i++)
                  r.accumulate(lfsv,i, - waterC * u * phi[i] * factor);
              }
              else
              {
                for (unsigned int i = 0; i < lfsv.size(); i++)
                  r.accumulate(lfsv,i,   waterC * u * phi[i] * factor);
              }
            }
          }

        /**
         * @brief Jacobian of volume term
         */
        template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
          void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
              M & mat) const
          {
            // get polynomial degree
            const int order    = lfsu.finiteElement().localBasis().order();
            const int intorder = intorderadd + quadrature_factor * order;

            // loop over quadrature points
            for (const auto& point : quadratureRule(eg.geometry(),intorder))
            {
              // evaluate basis functions
              std::vector<Scalar> phi(lfsu.size());
              lfsu.finiteElement().localBasis().evaluateFunction(point.position(),phi);

              const RF waterC = param.waterContent(eg.entity(),point.position(),time);

              // integration factor
              RF factor = point.weight() * eg.geometry().integrationElement(point.position());

              // integrate phi_j*phi_i
              if (DirectionType::isAdjoint())
              {
                for (unsigned int j = 0; j < lfsu.size(); j++)
                  for (unsigned int i = 0; i < lfsv.size(); i++)
                    mat.accumulate(lfsv,i,lfsu,j, - waterC * phi[j] * phi[i] * factor);
              }
              else
              {
                for (unsigned int j = 0; j < lfsu.size(); j++)
                  for (unsigned int i = 0; i < lfsv.size(); i++)
                    mat.accumulate(lfsv,i,lfsu,j,   waterC * phi[j] * phi[i] * factor);
              }
            }
          }

        //! set time
        void setTime (RF t)
        {
          time = t;
        }

        RF suggestTimestep(RF dt) const
        {
          // don't influence CFL condition
          return std::numeric_limits<RF>::max();
        }

      };

  }
}

#endif // DUNE_INVERSION_TRANSPORT_OPERATOR_DG_HH
