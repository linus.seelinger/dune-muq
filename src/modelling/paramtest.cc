#ifdef HAVE_CONFIG_H
#include "config.h"     
#endif

#if HAVE_LINUSVERSION

#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/timer.hh>
#include<dune/common/parametertreeparser.hh>

#include<problem.hh>
#include<src/groundwater/traits.hh>
#include<src/transport/traits.hh>



#include "MUQ/SamplingAlgorithms/SLMCMC.h"
#include "MUQ/SamplingAlgorithms/GreedyMLMCMC.h"
#include "MUQ/SamplingAlgorithms/MIMCMC.h"

#include "MUQ/Modeling/Distributions/Gaussian.h"
#include "MUQ/Modeling/Distributions/Density.h"

#include "MUQ/SamplingAlgorithms/MHKernel.h"
#include "MUQ/SamplingAlgorithms/MHProposal.h"
#include "MUQ/SamplingAlgorithms/CrankNicolsonProposal.h"
#include "MUQ/SamplingAlgorithms/SamplingProblem.h"
#include "MUQ/SamplingAlgorithms/SubsamplingMIProposal.h"

#include "MUQ/SamplingAlgorithms/MIComponentFactory.h"

#include <boost/property_tree/ptree.hpp>
#include "MUQ/SamplingAlgorithms/ParallelMIMCMCWorker.h"
#include "MUQ/SamplingAlgorithms/ParallelFixedSamplesMIMCMC.h"

namespace pt = boost::property_tree;
using namespace muq::Modeling;
using namespace muq::SamplingAlgorithms;
using namespace muq::Utilities;

#include"samplingproblem.hh"

int main(int argc, char** argv)
{
  spdlog::set_level(spdlog::level::debug);
  try{
    //Initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

    pt::ptree pt;
    pt.put("NumSamples", 1e3); // number of samples for single level
    pt.put("MCMC.NumSamples", 1e3); // number of samples for single level
    //pt.put("MCMC.burnin", 100); // number of samples for single level
    pt.put("MCMC.burnin", 1e2); // number of samples for single level
    pt.put("MLMCMC.Subsampling", 0);

    //auto comm = std::make_shared<parcer::Communicator>();

    auto comm = std::make_shared<parcer::Communicator>();

    auto componentFactory = std::make_shared<MyMIComponentFactory>(pt, helper, comm);
{
    componentFactory->SetComm(std::make_shared<parcer::Communicator>(MPI_COMM_SELF));
    auto problem = componentFactory->SamplingProblem(componentFactory->FinestIndex());

    Eigen::VectorXd mu(ParamDim(componentFactory->FinestIndex()));
    mu.setZero();
    //for (int i = 0; i < ParamDim(componentFactory->FinestIndex()); i++)
    //  mu(i) = .1;
    //problem->LogDensity(0, std::make_shared<SamplingState>(mu),AbstractSamplingProblem::SampleType::Accepted);
    for (int i = 0; i < ParamDim(componentFactory->FinestIndex()); i++)
      mu(i) = .1 + (double)i / 200.0;
    problem->LogDensity(0, std::make_shared<SamplingState>(mu),AbstractSamplingProblem::SampleType::Accepted);
    //problem->LogDensity(0, std::make_shared<SamplingState>(mu),AbstractSamplingProblem::SampleType::Accepted);
    //problem->LogDensity(0, std::make_shared<SamplingState>(mu),AbstractSamplingProblem::SampleType::Accepted);
    for (int i = 0; i < ParamDim(componentFactory->FinestIndex()); i++)
      mu(i) = .2;
    //problem->LogDensity(0, std::make_shared<SamplingState>(mu),AbstractSamplingProblem::SampleType::Accepted);
}



    // test passed
    return 0;

  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
}

#endif
