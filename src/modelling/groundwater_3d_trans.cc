#ifdef HAVE_CONFIG_H
#include "config.h"     
#endif

#if HAVE_LINUSVERSION

#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/timer.hh>
#include<dune/common/parametertreeparser.hh>

#include<problem.hh>
#include<src/groundwater/traits.hh>
#include<src/transport/traits.hh>
#include<driver.hh>

int main(int argc, char** argv)
{
  try{
    //Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    if(Dune::MPIHelper::isFake)
      std::cout<< "This is a sequential program." << std::endl;
    else
    {
      if(helper.rank()==0)
        std::cout << "parallel run on " << helper.size() << " processes" << std::endl;
    }

    if (argc > 2)
    {
      if(helper.rank()==0)
        std::cout << "usage: ./inversion (uses inversion.ini) or ./inversion <ini file>" << std::endl;
      return 1;
    }

    using DF = double;
    using RF = double;

    try
    {
      std::ifstream testFile("inversion.ini");
    }
    catch(...)
    {
      std::cerr << "main couldn't access files" << std::endl;
    }

    Dune::ParameterTree config;
    Dune::ParameterTreeParser parser;
    parser.readINITree("inversion.ini",config);

    std::vector<RF> extensions      = config.get<std::vector<RF>           >("grid.extensions");
    std::vector<unsigned int> cells = config.get<std::vector<unsigned int> >("grid.cells");
    unsigned int levels             = config.get<unsigned int              >("grid.levels",1);

    if (extensions.size() != cells.size())
    {
      DUNE_THROW(Dune::Exception,"cell and extension vectors differ in size");    
    }

    std::vector<unsigned int> minCells = cells;
    for (unsigned int i = 0; i < levels - 1; i++)
      for (unsigned int j = 0; j < cells.size(); j++)
      {
        if (minCells[j]%2 != 0)
          DUNE_THROW(Dune::Exception,"cannot create enough levels for hierarchical grid, check number of cells");

        minCells[j] /= 2;
      }

    if (helper.rank()==0)
    {
      for (unsigned int i = 0; i < extensions.size(); i++)
      {
        std::cout << extensions[i] << "\t" << cells[i] << std::endl;
      }
    }

#if HAVE_MPI

    // 2D
    if (extensions.size() == 2)
    {
      DUNE_THROW(Dune::Exception,"using 3d programm with 2d ini file");
    }

    // 3D
    else
    {
      using GridTraits      = GridTraits<DF,RF,3>;
      using InversionTraits = InversionTraits<GridTraits>;

      driver<InversionTraits,ModelType::Groundwater,Dune::Modelling::Formulation::Transient>(helper,config);
    }
#endif

    // test passed
    return 0;

  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
} 

#endif
