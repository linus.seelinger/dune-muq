#ifdef HAVE_CONFIG_H
#include "config.h"     
#endif

#if HAVE_LINUSVERSION

#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/timer.hh>
#include<dune/common/parametertreeparser.hh>

#include<problem.hh>
#include<src/groundwater/traits.hh>
#include<src/transport/traits.hh>



#include "MUQ/SamplingAlgorithms/SLMCMC.h"
#include "MUQ/SamplingAlgorithms/GreedyMLMCMC.h"
#include "MUQ/SamplingAlgorithms/MIMCMC.h"

#include "MUQ/Modeling/Distributions/Gaussian.h"
#include "MUQ/Modeling/Distributions/Density.h"

#include "MUQ/SamplingAlgorithms/MHKernel.h"
#include "MUQ/SamplingAlgorithms/MHProposal.h"
#include "MUQ/SamplingAlgorithms/CrankNicolsonProposal.h"
#include "MUQ/SamplingAlgorithms/SamplingProblem.h"
#include "MUQ/SamplingAlgorithms/SubsamplingMIProposal.h"

#include "MUQ/SamplingAlgorithms/MIComponentFactory.h"

#include <boost/property_tree/ptree.hpp>
#include "MUQ/SamplingAlgorithms/ParallelMIMCMCWorker.h"
#include "MUQ/SamplingAlgorithms/ParallelFixedSamplesMIMCMC.h"

namespace pt = boost::property_tree;
using namespace muq::Modeling;
using namespace muq::SamplingAlgorithms;
using namespace muq::Utilities;

#include"samplingproblem.hh"


    class MyStaticLoadBalancer : public StaticLoadBalancer {
    public:
      void setup(std::shared_ptr<ParallelizableMIComponentFactory> componentFactory, uint availableRanks) override {
        ranks_remaining = availableRanks;
        spdlog::info("Balancing load across {} ranks", availableRanks);
        auto indices = MultiIndexFactory::CreateFullTensor(componentFactory->FinestIndex()->GetVector());
        models_remaining = indices->Size();
      }

      int numCollectors(std::shared_ptr<MultiIndex> modelIndex) override {
        ranks_remaining--;
        return 1;
      }
      WorkerAssignment numWorkers(std::shared_ptr<MultiIndex> modelIndex) override {


        WorkerAssignment assignment;
        assignment.numWorkersPerGroup = 1;

        assignment.numGroups = ranks_remaining; // FIXME: This is only for 1lvl


        ranks_remaining -= assignment.numGroups * assignment.numWorkersPerGroup;
        return assignment;



        /*WorkerAssignment assignment;
        assignment.numWorkersPerGroup = 1;

        if (modelIndex->GetValue(0) == 0)
          assignment.numGroups = ranks_remaining / 5;
        else
          assignment.numGroups = ranks_remaining;

        
        ranks_remaining -= assignment.numGroups * assignment.numWorkersPerGroup;
        return assignment;*/
      }
    private:
      uint ranks_remaining;
      uint models_remaining;

    };


int main(int argc, char** argv)
{
  spdlog::set_level(spdlog::level::debug);
  try{
    //Initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

    pt::ptree pt;
    pt.put("NumSamples", 1e3); // number of samples for single level
    pt.put("MCMC.NumSamples", 1e3); // number of samples for single level
    //pt.put("MCMC.burnin", 100); // number of samples for single level
    pt.put("MCMC.burnin", 1e1); // number of samples for single level
    pt.put("MLMCMC.Subsampling", 0);

    //auto comm = std::make_shared<parcer::Communicator>();

    auto comm = std::make_shared<parcer::Communicator>();

    auto componentFactory = std::make_shared<MyMIComponentFactory>(pt, helper, comm);
/*{
  std::cout << "destroytest" << std::endl;
    componentFactory->SetComm(std::make_shared<parcer::Communicator>(MPI_COMM_SELF));
    auto problem = componentFactory->SamplingProblem(componentFactory->FinestIndex());
  std::cout << "destroytest done" << std::endl;
}*/

    /*StaticLoadBalancingMIMCMC parallelMIMCMC (pt, componentFactory);
    if (comm->GetRank() == 0) {
      parallelMIMCMC.Run();
      Eigen::VectorXd meanQOI = parallelMIMCMC.MeanQOI();
      std::cout << "mean QOI: " << meanQOI.transpose() << std::endl;
    }
    parallelMIMCMC.Finalize();*/

    StaticLoadBalancingMIMCMC parallelMIMCMC (pt, componentFactory, std::make_shared<MyStaticLoadBalancer>());

    if (comm->GetRank() == 0) {
      spdlog::info("Starting parallel run");
      parallelMIMCMC.Run();
      spdlog::info("Parallel run finished");
      Eigen::VectorXd meanQOI = parallelMIMCMC.MeanQOI();
      std::cout << "mean QOI: " << meanQOI.transpose() << std::endl;

      componentFactory->SetComm(std::make_shared<parcer::Communicator>(MPI_COMM_SELF));
      std::shared_ptr<MySamplingProblem> problem = std::dynamic_pointer_cast<MySamplingProblem>(componentFactory->SamplingProblem(componentFactory->FinestIndex()));
      problem->LogDensity(0, std::make_shared<SamplingState>(meanQOI, 1.0), AbstractSamplingProblem::SampleType::Accepted);
      problem->writeVTKfromQOI(meanQOI, "ml_mean");
    }
    parallelMIMCMC.Finalize();

    MPI_Finalize();


    /*
    SLMCMC slmcmc (pt, componentFactory);
    slmcmc.Run();

    auto problem = componentFactory->SamplingProblem(componentFactory->FinestIndex());
    Eigen::VectorXd meanQOI = slmcmc.MeanQOI();
    std::cout << "QOI:" << std::endl;
    std::cout << meanQOI << std::endl;
    problem->LogDensity(0, std::make_shared<SamplingState>(meanQOI, 1.0), AbstractSamplingProblem::SampleType::Accepted);
*/

    //MIMCMC mimcmc (pt, componentFactory);
    //mimcmc.Run();


    // test passed
    return 0;

  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
}

#endif
